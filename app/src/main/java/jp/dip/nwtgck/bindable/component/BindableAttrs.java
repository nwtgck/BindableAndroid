package jp.dip.nwtgck.bindable.component;

import android.util.AttributeSet;

import java.util.HashMap;
import java.util.Set;

import jp.dip.nwtgck.bindable.util.Util;

/**
 * Created by Jimmy on 2015/03/29.
 */
public class BindableAttrs extends HashMap<String, String>{

    /**
     * ViewのAttributeSetからnd_で始まる属性とその値を文字列で取得
     * @param attrs
     */
    public BindableAttrs(AttributeSet attrs){
        if(attrs != null) {
            int count = attrs.getAttributeCount();
            for (int i = 0; i < count; i++) {
                // 属性名を取得
                String attrName = attrs.getAttributeName(i);
                // その属性の値を取得
                Object value = attrs.getAttributeValue(i);

                // ND_PREFIXで始まっていれば
                if (attrName.indexOf(Util.ND_ATTR_PREFIX) == 0) {
                    // プレフィックスなしを属性名とする
                    String withoutPrefix = attrName.substring(Util.ND_ATTR_PREFIX.length());
                    put(withoutPrefix, value.toString());
                }

            }
        }
    }

    /**
     * 属性名を取得
     * @return
     */
    public Set<String> getAttrNames(){
        return keySet();
    }

    /**
     * 属性名からその値を習得
     * @param attrName
     * @return
     */
    public String getValueString(String attrName){
        return get(attrName);
    }
}
