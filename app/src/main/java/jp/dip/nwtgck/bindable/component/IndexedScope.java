package jp.dip.nwtgck.bindable.component;

import android.widget.Toast;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import jp.dip.nwtgck.bindable.iview.IEditableBindaleView;
import jp.dip.nwtgck.bindable.util.BindableViewUtil;

/**
 * Created by Jimmy on 2015/04/03.
 *
 * ListViewの項目一つ一つに割り振るスコープで、
 */
public final class IndexedScope extends Scope{
    private Arrangement mArrangement;
    private int mIndex;

    // スコープ内で使える変数（indexなど）
    private Map<String, Object> mInnerValues;

    public IndexedScope(Scope parentScope, Arrangement arrangement, final int index){
        // 親スコープをセット
        super(parentScope);

        mArrangement = arrangement;
        mIndex = index;

        mInnerValues = new HashMap<String, Object>(){{
            put("index", index);
            put("even", index % 2 == 0);
            put("odd", index % 2 != 0);
        }};
    }

    @Override
    public Object getValueByFieldName(String name) throws NoSuchFieldException, IllegalAccessException {
        String arrangeName = mArrangement.getName();
        // arrangementの名前と等しいかったら、
        if(name.equals(arrangeName)){
            // mArrangementのmIndex番目の値を返す
            return mArrangement.get(mIndex);

        // indexedScopeにある"index"など変数にアクセスしているなら、
        } else if(mInnerValues.containsKey(name)){
               return mInnerValues.get(name);
        } else {
           // 親スコープから値を返す
           return getParentScope().getValueByFieldName(name);
        }
    }

    @Override
    public void setValueByModelName(String modelName, Object value) throws NoSuchFieldException, IllegalAccessException {
        String arrangeName = mArrangement.getName();
        // arrangementの名前と等しいかったら、
        String[] splited = modelName.split("\\.");
        if(splited[0].equals(arrangeName)){
            if(splited.length == 1) {
                // mArrangementのmIndex番目の値に入れる
                mArrangement.set(mIndex, (Serializable) value);
                int mm = "".length();
            } else {
                // Arrangementの値を取得
                Object instance = mArrangement.get(mIndex);
                Field field = instance.getClass().getDeclaredField(splited[1]);
                field.setAccessible(true);
                // ドット演算子が続いていれば、
                for (int i = 2; i < splited.length; i++) {
                    // フィールドの値を取得
                    instance = field.get(instance);
                    // i番目のフィールドをinstanceから取得
                    field = instance.getClass().getDeclaredField(splited[i]);
                    field.setAccessible(true);
                }

                field.set(instance, value);
            }
        } else {
            // 親スコープのフィールドに入れる
            getParentScope().setValueByModelName(modelName, value);
        }
    }

    /**
     * arrangementを更新する
     * @param arrangement
     */
    public void updateArrangement(Arrangement arrangement){
        mArrangement = arrangement;
    }

//    @Override
//    public void notifyEvent(String eventName, IBindableView srcView) {
//        // イベントの通達はparentScopeにする
//        if()
//        getParentScope().notifyEvent(eventName, srcView);
//    }


    @Override
    public void notifyModelChanged(IEditableBindaleView changedView) {
        super.notifyModelChanged(changedView);

        // 変更したモデル名を取得
        String changedModelName = BindableViewUtil.getModelName(changedView);
        // モデル名からフィールド名を取得
        String fieldName = changedModelName.split("\\.")[0];
        // 変更したフィールドがarrangementなら
        if(fieldName.equals(mArrangement.getName())){
            // 現在のarrangementの配列またはリストの値を取得
            Object nowValue = mArrangement.getNowValue();
            // 配列またはリストの名前
            String listName = mArrangement.getListName();
            getParentScope().notifyModelChanged(changedView, listName, nowValue, MAX_DEEP, false);
        }
    }
}
