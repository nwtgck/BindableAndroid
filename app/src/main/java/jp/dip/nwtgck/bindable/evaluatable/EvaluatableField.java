package jp.dip.nwtgck.bindable.evaluatable;

import android.util.Log;

import java.lang.reflect.Field;

import jp.dip.nwtgck.bindable.util.Util;

/**
 * Created by Jimmy on 2015/04/01.
 */
public class EvaluatableField extends Evaluatable {
    private EvaluatableExpression mInstanceExp;
    private String mFieldName;

    public EvaluatableField(EvaluatableExpression instanceExp, String fieldName){
        mInstanceExp = instanceExp;
        mFieldName = fieldName;
    }

    @Override
    public Object eval() {
        try {
            // 評価する
            Object evaled = mInstanceExp.eval();
            // 「left.fieldName」の形式でデータを取得
            Field field = evaled.getClass().getDeclaredField(mFieldName);
            return field.get(evaled);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            String msg = String.format("[field %s not found]", mFieldName);
            Log.e(Util.LOG_ERROR_TAG, msg);
            return null;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            String msg = String.format("[field %s not found]", mFieldName);
            Log.e(Util.LOG_ERROR_TAG, msg);
            return null;
        }
    }

    @Override
    public boolean isChangable() {
        return false;
    }
}
