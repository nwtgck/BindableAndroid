package jp.dip.nwtgck.bindable.evaluatable;

import jp.dip.nwtgck.bindable.component.Operator;

/**
 * Created by Jimmy on 2015/04/01.
 *
 * 2項の式の構造
 */
public class EvaluatableBiominal extends Evaluatable {
    private EvaluatableExpression mLeft;
    private EvaluatableExpression mRight;
    private String mOperator;

    public EvaluatableBiominal(EvaluatableExpression left, EvaluatableExpression right, String operator){
        mLeft = left;
        mRight = right;
        mOperator = operator;
    }

    /**
     * 式を評価して返す
     * @return
     */
    @Override
    public Object eval() {
        return Operator.calc(mLeft.eval(), mRight.eval(), mOperator);
    }

    @Override
    public boolean isChangable() {
        return mLeft.isChangable() || mRight.isChangable();
    }
}
