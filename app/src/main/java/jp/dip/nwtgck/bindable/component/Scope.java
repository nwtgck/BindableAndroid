package jp.dip.nwtgck.bindable.component;

import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Log;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.dip.nwtgck.bindable.evaluatable.EvaluatableExpression;
import jp.dip.nwtgck.bindable.iview.IBindableView;
import jp.dip.nwtgck.bindable.iview.IEditableBindaleView;
import jp.dip.nwtgck.bindable.util.BindableViewUtil;
import jp.dip.nwtgck.bindable.util.Util;

/**
 * Created by Jimmy on 2015/03/28.
 */
public class Scope{
    // notifyModelChanged()の再帰の深さ
    protected final int MAX_DEEP = 10;

    // モデル名とバインドした属性とビューを取得できるように
    private Map<String, List<Triple<IBindableView, String, EvaluatableExpression>>> mFieldName2ViewWithAttr = new HashMap<>();

    // いつviewが自分自身で変更したか
    private Map<IBindableView, Long> mView2UpdatedTime = new HashMap<>();

//    // 式の評価結果を格納しておく
//    private Map<EvaluatableExpression, Object> mExp2Value = new HashMap<>();

    // 「イベントではなくメソッドや関数を持った式」が使われているビューと属性を取得　構造：式 -> [view, attrName]のリスト
    private Map<EvaluatableExpression, List<Pair<IBindableView, String>>> withFuncNotEventExp2ViewAndAttrName = new HashMap<>();

    // スコープ内のフィールとメソッド
    private Map<String, Field> mFieldName2Field = new HashMap<>();
    private Set<Method> mMethods = new HashSet<Method>();

//
//    // モデル名から値を取得するために、（モデル名に変更があったときに更新する）
//    private HashMap<String, Object> mModelName2Value = new HashMap<>();

    // 親スコープ
    private Scope mParentScope;

    // 子のスコープ
    private Set<Scope> mChildScopes = new HashSet<>();


    // onClickイベント
    public static final String EVENT_CLICK = "click";

    // インスタンス化された数、IDを振るためにつかう
    static long instanceCount = 0;

    // スコープのID　
    private long mId;

    // 式から評価した値を取り出すために
    private Map<EvaluatableExpression, Object> mWithFuncNotEventExp2Value = new HashMap<>();

    // フィールド名から値を取り出すために
    private Map<String, Object> mFieldName2Value = new HashMap<>();

    /**
     * 親を持たないスコープを作成
     */
    public Scope(){
        this(null);
    }

    public Scope(@Nullable Scope parentScope){
        // フィールドとメソッドを入れる
        Field[] fields = getClass().getFields();
        Field[] declaredFields = getClass().getDeclaredFields();
        Method[] methods = getClass().getMethods();
        Method[] declaredMethods = getClass().getDeclaredMethods();

        // フィールド名をキーにしてフィールドを取得できるように登録する
        for(Field field: fields){
            field.setAccessible(true);
            mFieldName2Field.put(field.getName(), field);
        }
        for(Field field: declaredFields){
            field.setAccessible(true);
            mFieldName2Field.put(field.getName(), field);
        }

        Collections.addAll(mMethods, methods);
        Collections.addAll(mMethods, declaredMethods);

        mId = ++instanceCount;
        mParentScope = parentScope;
    }



    @Override
    public int hashCode() {
        return (int) mId;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Scope){
            Scope that = (Scope)o;
            return this.mId == that.mId;
        } else {
            return false;
        }
    }

    /**
     * アクティビティのviewとScopeの値のバインドが終了した時に呼ばれる
     */
    public void onEndBindActivity(){
        /**
         * フィールドの値の登録しておく
         */
        for(String fieldName: mFieldName2ViewWithAttr.keySet()){
            try {
                Object value = getValueByFieldName(fieldName);
                mFieldName2Value.put(fieldName, value == null? null: Util.clone((Serializable)value) );
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * フィールドを名前から取得する
     * @param name
     * @return
     * @throws NoSuchFieldException
     */
    private Field getField(String name) throws NoSuchFieldException {
        if(mFieldName2Field.containsKey(name)){
            return mFieldName2Field.get(name);
        }
        throw new NoSuchFieldException(String.format("field %s not found", name));
    }

    /**
     * フィールド名を指定して値を取得
     * @param name
     * @return
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public Object getValueByFieldName(String name) throws IllegalAccessException, NoSuchFieldException {
        try {
            Field field = this.getField(name);
            return field.get(this);
        } catch (NoSuchFieldException e) {
            // 親スコープを持っていれば、
            if(mParentScope != null){
                // 親スコープで探してみる
                return mParentScope.getValueByFieldName(name);
            }
        }

        // 親スコープを探しても見つからなければ、エラー
        throw new NoSuchFieldException();
    }

    /**
     * モデル名を指定して、値を更新
     * @param name
     * @param value
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public void setValueByModelName(String name, Object value) throws IllegalAccessException, NoSuchFieldException {
        setFieldValue(name, value);
    }


    /**
     * メソッドを名前と引数の型から取得する
     * @param name
     * @param paramTypes
     * @return
     * @throws NoSuchMethodException
     */
    public Method getMethod(String name, Class... paramTypes) throws NoSuchMethodException {
        for(Method method: mMethods){
            // 名前が等しく、
            if(method.getName().equals(name)){
                // 引数の型が等しい
                Class[] types = method.getParameterTypes();
                if(types.length == paramTypes.length) {
                    int i;
                    for (i = 0; i < types.length && Util.equalType(types[i], paramTypes[i]); i++);
                    if(i == types.length){
                        // アクセス可能にして返す
                        method.setAccessible(true);
                        return method;
                    }
                }

            }
        }
        throw new NoSuchMethodException(String.format("method %s not found", name));
    }

    /**
     * モデル名に対応したビューの属性
     * @param modelName
     * @param attrName
     * @param bindableView
     */
    public void enrollAttrAndModelAndExpression(String modelName, IBindableView bindableView, String attrName, EvaluatableExpression expression){
        // 追加するタプルを作成
        Triple<IBindableView, String, EvaluatableExpression> viewAndAttrAndExp = new Triple<>(bindableView, attrName, expression);

        /* モデル名からビューと属性を取り出せるようにする */
        // すでに存在するモデル名なら
        if(mFieldName2ViewWithAttr.keySet().contains(modelName)){
            // 属性を指定してviewを追加
            mFieldName2ViewWithAttr.get(modelName).add(viewAndAttrAndExp);
        } else {
            // listのインスタンスを作って属性付きviewを追加する
            ArrayList<Triple<IBindableView, String, EvaluatableExpression>> viewsWithAttrs = new ArrayList<>();
            viewsWithAttrs.add(viewAndAttrAndExp);
            mFieldName2ViewWithAttr.put(modelName, viewsWithAttrs);
        }
    }

    /**
     * 式からビューと属性が取得できるように登録する
     * @param expression
     * @param bindableView
     * @param attrName
     */
    public void enrollWithFuncExp2ViewAndAttrName(EvaluatableExpression expression, IBindableView bindableView, String attrName){
        /* 式からビューと属性を取り出せるようにする */
        // すでに存在する式なら
        boolean b = expression.isChangable();
        Pair<IBindableView, String> viewAndAttr = new Pair<>(bindableView, attrName);
        if(withFuncNotEventExp2ViewAndAttrName.containsKey(expression)){
            // 式を指定して追加
            withFuncNotEventExp2ViewAndAttrName.get(expression).add(viewAndAttr);

            // 式がメソッドや関数を含んでいて、イベントではなかったら
        } else if(isWithFuncNotEventExpression(expression, attrName)){
            // リストを作成して
            ArrayList<Pair<IBindableView, String>> viewAndAttrs = new ArrayList<>();
            // リストに追加して
            viewAndAttrs.add(viewAndAttr);
            // withFuncExp2ViewAndAttrNameに登録する
            withFuncNotEventExp2ViewAndAttrName.put(expression, viewAndAttrs);
        }
    }

    /**
     * メソッドや関数を含んでいない式に対する評価した値を登録する
     * @param expression
     * @param evaledValue
     * @param attrName
     */
    public void enrollWithFuncExp2Value(EvaluatableExpression expression, Object evaledValue, String attrName){
        // 式がメソッドや関数を含んでいて、イベントではなかったら
        if(isWithFuncNotEventExpression(expression, attrName)){
            // 式から値が取れるように登録する
            mWithFuncNotEventExp2Value.put(expression, Util.clone((Serializable)evaledValue));
        }
    }

    // 式がメソッドや関数を含んでいて、イベントでないかどうか
    private boolean isWithFuncNotEventExpression(EvaluatableExpression expression, String attrName){
        return expression.isChangable() && !BindableViewUtil.isEventAttr(attrName);
    }

    /**
     * 属性から発火するモデルの変更を、他のviewに反映させる
     * @param changedView
     */
    public void notifyModelChanged(final IEditableBindaleView changedView){
        // モデル名とモデルの値を取得
        String changedModelName = BindableViewUtil.getModelName(changedView);
        Object model = changedView.getNdModel();

        // viewの変更時間を記録
        mView2UpdatedTime.put(changedView, System.currentTimeMillis());

        // 更新を反映させる
        notifyModelChanged(changedView, changedModelName, model, MAX_DEEP, false);
    }

    protected void notifyModelChanged(@Nullable IBindableView changedView, String changedModelName, Object newValue, int deep, boolean fromParent){

        // フィールド名取得
        String fieldName = changedModelName.split("\\.")[0];

        try {
            // スコープ内の変数を更新する（[順序]　保存されたフィールドの値と現在のフィールドの値が等しければ、終了より前に書かないといけない）
            setValueByModelName(changedModelName, newValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            // 保存されているフィールドの値を取得
            Object oldFieldVal = mFieldName2Value.get(fieldName);
            // 現在のフィールドの値を取得
            Object nowFieldVal = getValueByFieldName(fieldName);
            // 保存されているフィールドの値と現在のフィールドの値が等しければ、終了する
            if(oldFieldVal == null? nowFieldVal == null: oldFieldVal.equals(nowFieldVal)){
                return;
            }
            // フィールドの値を保存
            mFieldName2Value.put(fieldName, Util.clone((Serializable)nowFieldVal));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

        // 親スコープから来てなく、親スコープに存在するモデル名なら、伝える
        if(!fromParent && mParentScope != null &&  mParentScope.mFieldName2ViewWithAttr.containsKey(changedModelName)){
            mParentScope.notifyModelChanged(changedView, changedModelName, newValue, deep, fromParent);
        }

        // 子のスコープに変更を伝える
        for(Scope childScope: mChildScopes){
            childScope.notifyModelChanged(changedView, changedModelName, newValue, deep, true);
        }

//
//        // モデルの変更を調べるために
//        Map<String, Integer> modelName2HashCode = getFieldName2HashCode();

        // 関数やメソッドを含んだ式を評価して、値が前と違っていれば更新
        checkIfUpdateFunction(changedView);

        // changedModelNameに結びついた、属性があるなら
        if(mFieldName2ViewWithAttr.keySet().contains(changedModelName)){

            /* modelに結びついたviewを属性を含め更新する */
            List<Triple<IBindableView, String, EvaluatableExpression>> viewsWithAttr = mFieldName2ViewWithAttr.get(changedModelName);
            for(Triple<IBindableView, String, EvaluatableExpression> viewWithAttr: viewsWithAttr){
                // ビューと属性名と式を評価した可能にものを取得
                IBindableView bindableView = viewWithAttr.first;
                String attrName = viewWithAttr.second;
                EvaluatableExpression expression = viewWithAttr.third;

                // バインドしている属性を更新する
               updateView(changedView, bindableView, attrName, expression);
            }

//
//            // 深くまで更新反映が続いてなければ、
//            if(deep != -1){
//                // 今のフィールドのハッシュを得る
//
//                // フィールド名を取得
//                String fieldName = changedModelName.split("\\.")[0];
//
//                // 更新前のフィールドの値とと現在のフィールドの値を比較する
//                for(String fName: mFieldName2Value.keySet()){
//
//                    try {
//                        Object oldValue = mFieldName2Value.get(fieldName);
//                        Object nowValue = getValueByFieldName(fieldName);
//
//                        // 更新前のハッシュと今のハッシュが変わっていれば
//                        if(oldValue == null? nowValue != null: !oldValue.equals(nowValue)){
//                            // 再帰的に更新する
//                            notifyModelChanged(null, fName, nowValue, deep-1, fromParent);
//                            mFieldName2Value.put(fName, Util.clone((Serializable)nowValue));
//
//                        }
//                    } catch (IllegalAccessException | NoSuchFieldException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }

        }

    }


    /**
     * viewと属性名と式を指定して更新する
     * @param srcView 変更元のview
     * @param bindableView 変更先のview
     * @param attrName
     * @param expression
     */
    private void updateView(@Nullable IBindableView srcView, IBindableView bindableView, String attrName, EvaluatableExpression expression){
        if(bindableView != srcView) { // 変更元のviewを変更しないように
            long nowMilli = System.currentTimeMillis();

            // 変更して間もない状態で、viewが更新されていれば、
            if(srcView != null && mView2UpdatedTime.containsKey(bindableView) && nowMilli - mView2UpdatedTime.get(bindableView) < 100){
                // 警告をだす
                Log.w(Util.LOG_WARNING_TAG, bindableView + "が間もない時間で"+srcView+"によって更新されています");
            }


            try {
                // 属性を更新するメソッドを取得
                Method updateAttrMethod = Util.getUpdateAttrMethod(bindableView, attrName);
                // 評価する
                Object evaledValue = expression.eval();
                // 属性の更新メソッドを呼ぶ
                updateAttrMethod.invoke(bindableView, evaledValue);

//                // 式の評価結果を保存
//                Object evaledClone = Util.clone((Serializable)evaledValue);
//                mExp2Value.put(expression, evaledClone);

            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }

            Log.d("changed", bindableView.getClass().getSimpleName());
        }

    }

    /**
     *
     * @param clonize valueをクローンするかどうか
     * @return
     */
    private HashMap<String, Object> getModelName2Value(boolean clonize){
        HashMap<String, Object> modelName2Value = new HashMap<>();
        for(String modelName: mFieldName2ViewWithAttr.keySet()){

            try {
                // フィールドの値を取得
                Object value = getValueByFieldName(modelName);
                // フィールドの値をクローンするなら
                if(clonize) {
                    if(value instanceof Serializable) {
                        // clone()を取得
                        Object clone = Util.clone((Serializable)value);
                        // モデルの名前とクローンしたものを組にして保存
                        modelName2Value.put(modelName, clone);
                    } else {
                        Log.e(Util.LOG_ERROR_TAG, String.format("%s is not implemented Serializable. Scope fields must be implemented Serializable!", modelName));
                    }
                } else {
                    // モデルの名前とその値を組にして保存
                    modelName2Value.put(modelName, value);
                }
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        // 親スコープがあるなら、
        if(mParentScope != null){
            // 親のフィールドの状態も追加する
            HashMap<String, Object> parentValues = mParentScope.getModelName2Value(clonize);
            for(String modelName: parentValues.keySet()){
                modelName2Value.put(modelName, parentValues.get(modelName));
            }
        }

        return modelName2Value;
    }

    /**
     * フィールド名からハッシュコード
     * @return
     */
    private Map<String, Integer> getFieldName2HashCode(){
        /**
         * フィールドの値のハッシュコードを入れる
         */
        Map<String, Integer> fieldName2HashCode = new HashMap<>();
        for(String modelName: mFieldName2ViewWithAttr.keySet()){
            try {
                Object value = getValueByFieldName(modelName);
                fieldName2HashCode.put(modelName, value == null? 0: value.hashCode());
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }

        }

        // 親スコープがあれば、親スコープのフィールドのハッシュコードもいれる
        if(mParentScope != null){
            Map<String, Integer> parentHashCodes = mParentScope.getFieldName2HashCode();
            fieldName2HashCode.putAll(parentHashCodes);
        }

        return fieldName2HashCode;
    }

    /**
     * メソッドや関数を含んでいる式を返す
     * @return
     */
    private Set<EvaluatableExpression> getWithFuncExpressions(){
        return withFuncNotEventExp2ViewAndAttrName.keySet();
    }

    /**
     *
     * @param eventName clickなどのイベント名
     * @param srcView イベントが発火したview
     */
    public void notifyEvent(String eventName, IBindableView srcView){
        // viewの属性を取得
        BindableAttrs attrs = srcView.getBindableAttrs();
        // change属性を取得
        String expString = attrs.getValueString(eventName);
        if(expString != null){
            // 式を作成する
            EvaluatableExpression expression = new EvaluatableExpression(this, expString);

//            // 更新前のフィールドのハッシュを取得
//            Map<String, Integer> modelName2HashCode = getFieldName2HashCode();

            // 評価する（イベントなので戻り値はいらない）
            expression.eval();

            // 関数やメソッドなどを再評価して、更新の必要があれば、更新する
            checkIfUpdateFunction(srcView);

            // 親スコープも関数やメソッドなどを再評価して、値が変更していれば、更新する
            if(mParentScope != null){
                mParentScope.checkIfUpdateFunction(srcView);
            }

            // フィールドが更新されているか確認して、更新されていれば更新反映する
            checkIfUpdateField(srcView);



//
//            // 今のフィールドのハッシュを得る
//            Map<String, Integer> nowModelName2HashCode = getFieldName2HashCode();



        }

    }

    /**
     * フィールドが更新されているか確認して、更新されていれば、
     * 更新反映する
     * @param srcView
     */
    private void checkIfUpdateField(IBindableView srcView){
        // 更新前のフィールドの値と現在のフィールドの値を比較する
        for(String filedName: mFieldName2Value.keySet()){
            try {
                Object oldValue = mFieldName2Value.get(filedName);
                Object nowValue = getValueByFieldName(filedName);
                // 更新前の値とと今の値が変わっていれば
                if(oldValue == null? nowValue != null: !oldValue.equals(nowValue)){
                    // 再帰的に更新する
                    notifyModelChanged(srcView, filedName, nowValue, MAX_DEEP, false);
                }
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        // 親が持っていれば、再帰的にチェックする
        if(mParentScope != null){
            mParentScope.checkIfUpdateField(srcView);
        }
    }

    /**
     * 親のスコープ追加
     * @return
     */
    public Scope getParentScope(){
        return mParentScope;
    }

    /**
     * 子のスコープを追加する
     * @param scope
     */
    public void addChildScope(Scope scope){
        mChildScopes.add(scope);
        int mm = 0;
    }

    /**
     * idを取得する
     * @return
     */
    public long getId() {
        return mId;
    }

    /**
     * 子スコープの削除
     * @param scope
     */
    public void removeChild(Scope scope){
        mChildScopes.remove(scope);
        int mm = 0;
    }

    /**
     * 関数やメソッドなど内部で変更があるかもしれないものを評価しなおして、
     * 値が変更していれば、更新する
     * @param srcView
     */
    private void checkIfUpdateFunction(IBindableView srcView){
        // 関数やメソッドを含んだ式を評価して、値が前と違っていれば更新
        for(EvaluatableExpression exp: mWithFuncNotEventExp2Value.keySet()){
            // 式を評価してみる
            Object evaledValue = exp.eval();
            Object oldValue = mWithFuncNotEventExp2Value.get(exp);
            // 以前の値と違っていれば、
            if(evaledValue == null? oldValue == null: !evaledValue.equals(oldValue)){
                // 式を指定して、ビューと属性を取得
                List<Pair<IBindableView, String>> viewAndAttrs = withFuncNotEventExp2ViewAndAttrName.get(exp);

                for(Pair<IBindableView, String> viewAndAttr: viewAndAttrs){
                    IBindableView bindableView = viewAndAttr.first;
                    String attrName = viewAndAttr.second;
                    // ビューを変更する
                    updateView(srcView, bindableView, attrName, exp);
                    // 式に対して新しい評価値を登録する
                    mWithFuncNotEventExp2Value.put(exp, Util.clone((Serializable)evaledValue));
                }
            }
        }
    }

    /**
     * ドット演算子があっても値をフィールドに値をセットできる
     * @param modelName
     * @param value
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    private void setFieldValue(String modelName, Object value) throws IllegalAccessException, NoSuchFieldException {

        try {
            Object instance = this;
            String[] splited = modelName.split("\\.");
            // スコープ内のフィールドを取得
            Field field = instance.getClass().getDeclaredField(splited[0]);
            field.setAccessible(true);

            // ドット演算子が続いていれば、
            for (int i = 1; i < splited.length; i++) {
                // フィールドの値を取得
                instance = field.get(instance);
                // i番目のフィールドをinstanceから取得
                field = instance.getClass().getDeclaredField(splited[i]);
                field.setAccessible(true);
            }

            // 値を更新
            field.set(instance, value);

        } catch (NoSuchFieldException e){
            // 存在しないフィールド名のときは、親スコープを参照する
            if(mParentScope != null){
                mParentScope.setFieldValue(modelName, value);
            } else {
                throw new NoSuchFieldException(modelName+ " not found");
            }
        }

    }
}
