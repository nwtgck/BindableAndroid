package jp.dip.nwtgck.bindable.component;

/**
 * Created by Jimmy on 2015/03/31.
 */
public class Triple<T1, T2, T3> {
    public T1 first;
    public T2 second;
    public T3 third;

    public Triple(T1 first, T2 second, T3 third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }
}
