package jp.dip.nwtgck.bindable.evaluatable;

import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

import jp.dip.nwtgck.bindable.component.Arrangement;
import jp.dip.nwtgck.bindable.component.Scope;

/**
 * Created by Jimmy on 2015/04/04.
 */
public class EvaluatableArrangement extends Evaluatable{
    private Scope mScope;
    private String mArrangeName;
    private EvaluatableExpression mArrangeExp;
    // 元になった配列やリストの名前
    @Nullable private String mListName;

    public EvaluatableArrangement(Scope scope, String arrangeName, EvaluatableExpression arrangementExp){
        mScope = scope;
        mArrangeName = arrangeName;
        mArrangeExp = arrangementExp;

        // TODO [他の方法] 方法が強引で、思うものが取得できない可能性がある
        /* 式に含んでいる配列名を取得 */
        // 式が関与しているモデル名を取得
        Set<String> bindingFieldNames = mArrangeExp.getBindingFieldNames();
        for(String fieldName: bindingFieldNames){
            try {
                // フィールドの値を取得
                Object value = mScope.getValueByFieldName(fieldName);
                // 値が配列かリストなら、listNameだと決めつける
                if(value instanceof ArrayList || value instanceof Object[]){
                    mListName = fieldName;
                    break;
                }
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object eval() {
        Object arrangmentObj = mArrangeExp.eval();

        if(arrangmentObj instanceof Serializable[]){
            return new Arrangement(mArrangeName, (Serializable[])arrangmentObj, mListName);
        } else if(arrangmentObj instanceof ArrayList){
            return new Arrangement(mArrangeName, (ArrayList)arrangmentObj, mListName);
        }
        return null;
    }

    @Override
    public boolean isChangable() {
        return mArrangeExp.isChangable();
    }


}
