package jp.dip.nwtgck.bindable.component;

/**
 * Created by Jimmy on 2015/03/31.
 *
 * ２つの値(Object)をとり、計算するメソッド群
 */
public class Operator {

    // 演算子
    public static final String OPE_DOT = ".";
    public static final String OPE_NOT = "!";
    public static final String OPE_COMMA = ",";
    public static final String OPE_MULTI = "*";
    public static final String OPE_DIVIDE = "/";
    public static final String OPE_MOD = "%";
    public static final String OPE_PLUS = "+";
    public static final String OPE_MINUS = "-";
    public static final String OPE_LESS = "<";
    public static final String OPE_GRATER = ">";
    public static final String OPE_LESS_EQUALS = "<=";
    public static final String OPE_GRATER_EQUALS = ">=";
    public static final String OPE_EQUALS = "==";
    public static final String OPE_NOT_EQUALS = "!=";
    public static final String OPE_AND_AND = "&&";
    public static final String OPE_BAR_BAR = "||";

    /**
     * 計算する
     * @param left
     * @param right
     * @return
     */
    public static Object calc(Object left, Object right, String operator) {
        if (operator.equals(OPE_NOT)) {
            return Operator.not(left, right);
        } else if (operator.equals(OPE_MULTI)) {
            return Operator.multi(left, right);
        } else if(operator.equals(OPE_DIVIDE)){
            return Operator.divide(left, right);
        } else if(operator.equals(OPE_MOD)){
            return Operator.mod(left, right);
        } else if(operator.equals(OPE_PLUS)){
            return Operator.plus(left, right);
        } else if(operator.equals(OPE_MINUS)){
            return Operator.minus(left, right);
        } else if(operator.equals(OPE_LESS)){
            return Operator.less(left, right);
        } else if(operator.equals(OPE_GRATER)){
            return Operator.grater(left, right);
        } else if(operator.equals(OPE_LESS_EQUALS)){
            return Operator.lessEquals(left, right);
        } else if(operator.equals(OPE_GRATER_EQUALS)){
            return Operator.graterEquals(left, right);
        } else if(operator.equals(OPE_EQUALS)){
            return Operator.equals(left, right);
        } else if(operator.equals(OPE_NOT_EQUALS)){
            return Operator.notEquals(left, right);
        } else if(operator.equals(OPE_AND_AND)){
            return Operator.andAnd(left, right);
        } else if(operator.equals(OPE_BAR_BAR)){
            return Operator.barBar(left, right);
        }

        return null;
    }

    /**
     * 否定する
     * @param blank 空（nullが入ると思う）
     * @param target
     * @return
     */
    public static boolean not(Object blank, Object target){
        if(target instanceof Boolean){
            return !(boolean)target;
        } else {
            String msg = "Be instance of boolean in not()";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 掛け算する
     * @param left
     * @param right
     * @return
     */
    public static Number multi(Object left, Object right) {
        if(left instanceof Integer && right instanceof Integer){
            return (int)left * (int)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (int)left * (long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (int)left * (double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (int)left * (float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (long)left * (int)right;
        } else if(left instanceof Long && right instanceof Long){
            return (long)left * (long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (long)left * (double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (long)left * (float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (double)left * (int)right;
        } else if(left instanceof Double && right instanceof Long){
            return (double)left * (long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (double)left * (double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (double)left * (float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (float)left * (int)right;
        } else if(left instanceof Float && right instanceof Long){
            return (float)left * (long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (float)left * (double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (float)left * (float)right;
        } else {
            String msg = "[*] Be instance of integer or double";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 割り算
     * @param left
     * @param right
     * @return
     */
    public static Number divide(Object left, Object right){
        if(left instanceof Integer && right instanceof Integer){
            return (int)left / (int)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (int)left / (long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (int)left / (double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (int)left / (float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (long)left / (int)right;
        } else if(left instanceof Long && right instanceof Long){
            return (long)left / (long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (long)left / (double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (long)left / (float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (double)left / (int)right;
        } else if(left instanceof Double && right instanceof Long){
            return (double)left / (long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (double)left / (double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (double)left / (float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (float)left / (int)right;
        } else if(left instanceof Float && right instanceof Long){
            return (float)left / (long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (float)left / (double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (float)left / (float)right;
        } else {
            String msg = "[/] Be instance of integer or double";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 剰余
     * @param left
     * @param right
     * @return
     */
    public static Number mod(Object left, Object right){
        if(left instanceof Integer && right instanceof Integer){
            return (int)left % (int)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (int)left % (long)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (long)left % (int)right;
        } else if(left instanceof Long && right instanceof Long){
            return (long)left % (long)right;
        } else {
            String msg = "Be instance of integer in mod()";
            throw new ArithmeticException(msg);
        }
    }


    /**
     * 足し算
     * どちらかが文字列の場合に文字列になる
     * @param left
     * @param right
     * @return
     */
    public static Object plus(Object left, Object right){
        if(left instanceof String || right instanceof String){
            return left.toString() + right.toString();
        } else if(left instanceof Integer && right instanceof Integer){
            return (int)left + (int)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (int)left + (long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (int)left + (double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (int)left + (float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (long)left + (int)right;
        } else if(left instanceof Long && right instanceof Long){
            return (long)left + (long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (long)left + (double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (long)left + (float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (double)left + (int)right;
        } else if(left instanceof Double && right instanceof Long){
            return (double)left + (long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (double)left + (double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (double)left + (float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (float)left + (int)right;
        } else if(left instanceof Float && right instanceof Long){
            return (float)left + (long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (float)left + (double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (float)left + (float)right;
        } else {
            String msg = "[+] Be instance of string or integer or double";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 引き算
     * @param left
     * @param right
     * @return
     */
    public static Number minus(Object left, Object right){
        if(left instanceof Integer && right instanceof Integer){
            return (int)left - (int)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (int)left - (long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (int)left - (double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (int)left - (float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (long)left - (int)right;
        } else if(left instanceof Long && right instanceof Long){
            return (long)left - (long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (long)left - (double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (long)left - (float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (double)left - (int)right;
        } else if(left instanceof Double && right instanceof Long){
            return (double)left - (long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (double)left - (double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (double)left - (float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (float)left - (int)right;
        } else if(left instanceof Float && right instanceof Long){
            return (float)left - (long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (float)left - (double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (float)left - (float)right;
        } else {
            String msg = "[-] Be instance of integer or double";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 小なり
     * @param left
     * @param right
     * @return
     */
    public static boolean less(Object left, Object right){
        if(left instanceof Integer && right instanceof Integer){
            return (Integer)left < (Integer)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (Integer)left < (Long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (Integer)left < (Double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (Integer)left < (Float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (Long)left < (Integer)right;
        } else if(left instanceof Long && right instanceof Long){
            return (Long)left < (Long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (Long)left < (Double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (Long)left < (Float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (Double)left < (Integer)right;
        } else if(left instanceof Double && right instanceof Long){
            return (Double)left < (Long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (Double)left < (Double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (Double)left < (Float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (Float)left < (Integer)right;
        } else if(left instanceof Float && right instanceof Long){
            return (Float)left < (Long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (Float)left < (Double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (Float)left < (Float)right;
        } else {
            String msg = "[<] Be instance of integer or double";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 大なり
     * @param left
     * @param right
     * @return
     */
    public static boolean grater(Object left, Object right){
        if(left instanceof Integer && right instanceof Integer){
            return (Integer)left > (Integer)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (Integer)left > (Long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (Integer)left > (Double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (Integer)left > (Float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (Long)left > (Integer)right;
        } else if(left instanceof Long && right instanceof Long){
            return (Long)left > (Long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (Long)left > (Double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (Long)left > (Float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (Double)left > (Integer)right;
        } else if(left instanceof Double && right instanceof Long){
            return (Double)left > (Long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (Double)left > (Double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (Double)left > (Float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (Float)left > (Integer)right;
        } else if(left instanceof Float && right instanceof Long){
            return (Float)left > (Long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (Float)left > (Double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (Float)left > (Float)right;
        } else {
            String msg = "[>] Be instance of integer or double";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 小なりイコール
     * @param left
     * @param right
     * @return
     */
    public static boolean lessEquals(Object left, Object right){
        if(left instanceof Integer && right instanceof Integer){
            return (Integer)left <= (Integer)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (Integer)left <= (Long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (Integer)left <= (Double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (Integer)left <= (Float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (Long)left <= (Integer)right;
        } else if(left instanceof Long && right instanceof Long){
            return (Long)left <= (Long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (Long)left <= (Double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (Long)left <= (Float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (Double)left <= (Integer)right;
        } else if(left instanceof Double && right instanceof Long){
            return (Double)left <= (Long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (Double)left <= (Double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (Double)left <= (Float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (Float)left <= (Integer)right;
        } else if(left instanceof Float && right instanceof Long){
            return (Float)left <= (Long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (Float)left <= (Double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (Float)left <= (Float)right;
        } else {
            String msg = "[<=] Be instance of integer or double";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * 大なりイコール
     * @param left
     * @param right
     * @return
     */
    public static boolean graterEquals(Object left, Object right){
        if(left instanceof Integer && right instanceof Integer){
            return (Integer)left >= (Integer)right;
        } else if(left instanceof Integer && right instanceof Long){
            return (Integer)left >= (Long)right;
        } else if(left instanceof Integer && right instanceof Double){
            return (Integer)left >= (Double)right;
        } else if(left instanceof Integer && right instanceof Float){
            return (Integer)left >= (Float)right;
        } else if(left instanceof Long && right instanceof Integer){
            return (Long)left >= (Integer)right;
        } else if(left instanceof Long && right instanceof Long){
            return (Long)left >= (Long)right;
        } else if(left instanceof Long && right instanceof Double){
            return (Long)left >= (Double)right;
        } else if(left instanceof Long && right instanceof Float){
            return (Long)left >= (Float)right;
        } else if(left instanceof Double && right instanceof Integer){
            return (Double)left >= (Integer)right;
        } else if(left instanceof Double && right instanceof Long){
            return (Double)left >= (Long)right;
        } else if(left instanceof Double && right instanceof Double){
            return (Double)left >= (Double)right;
        } else if(left instanceof Double && right instanceof Float){
            return (Double)left >= (Float)right;
        } else if(left instanceof Float && right instanceof Integer){
            return (Float)left >= (Integer)right;
        } else if(left instanceof Float && right instanceof Long){
            return (Float)left >= (Long)right;
        } else if(left instanceof Float && right instanceof Double){
            return (Float)left >= (Double)right;
        } else if(left instanceof Float && right instanceof Float){
            return (Float)left >= (Float)right;
        } else {
            String msg = "[>=] Be instance of integer or double";
            throw new ArithmeticException(msg);
        }
    }


    /**
     * 等しいかどうか
     * @param left
     * @param right
     * @return
     */
    public static boolean equals(Object left, Object right){
        return (left == null)? right == null: left.equals(right);
    }


    /**
     * 等しくないかどうか
     * @param left
     * @param right
     * @return
     */
    public static boolean notEquals(Object left, Object right){
        return !equals(left, right);
    }


    /**
     * かつ：論理
     * @param left
     * @param right
     * @return
     */
    public static boolean andAnd(Object left, Object right){
        if(left instanceof Boolean && right instanceof Boolean){
            return (boolean)left && (boolean)right;
        } else {
            String msg = "Be instance of boolean in andAnd()";
            throw new ArithmeticException(msg);
        }
    }

    /**
     * または：論理
     * @param left
     * @param right
     * @return
     */
    public static boolean barBar(Object left, Object right){
        if(left instanceof Boolean && right instanceof Boolean){
            return (boolean)left || (boolean)right;
        } else {
            String msg = "Be instance of boolean in barBar()";
            throw new ArithmeticException(msg);
        }
    }
}
