package jp.dip.nwtgck.bindable.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.Switch;

import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.iview.IEditableBindaleView;

/**
 * Created by Jimmy on 2015/03/30.
 */
public class BindableSwitch extends Switch implements IEditableBindaleView{
    private BindableAttrs mBindableAttrs;

    public BindableSwitch(Context context) {
        super(context);
        mBindableAttrs = null;
    }

    public BindableSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        mBindableAttrs = new BindableAttrs(attrs);
    }

    public BindableSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBindableAttrs = new BindableAttrs(attrs);
    }

    @Override
    public Object getNdModel() {
        return isChecked();
    }

    @Override
    public <S extends Scope> void defModelChanged(final S scope) {
        // チェックが変更した時に変更を知らせる
        setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                scope.notifyModelChanged(BindableSwitch.this);
            }
        });
    }

    @Override
    public void setNdModel(Object model) {
        if(model instanceof Boolean){
            setChecked((Boolean)model);
        } else {
            throw new IllegalArgumentException("model must be instance of boolean in BindableSwitch");
        }
    }

    @Override
    public BindableAttrs getBindableAttrs() {
        return mBindableAttrs;
    }

    @Override
    public void onBind(Scope scope) {

    }

    @Override
    public void onStartBinding(Scope scope) {

    }
}
