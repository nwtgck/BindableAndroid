package jp.dip.nwtgck.bindable.evaluatable;

/**
 * Created by Jimmy on 2015/03/31.
 *
 * Expressionが評価可能な状態になったら、これを無名で実装して返す
 * スコープを参照する場合はgetValue()の値はスコープに依存する
 */
abstract public class Evaluatable {
    abstract public Object eval();

    // メソッドや関数を持っているか
    abstract public boolean isChangable();
}
