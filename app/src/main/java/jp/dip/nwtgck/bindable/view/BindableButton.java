package jp.dip.nwtgck.bindable.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;

import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.iview.IBindableView;
import jp.dip.nwtgck.bindable.util.BindableViewUtil;

/**
 * Created by Jimmy on 2015/04/01.
 */
public class BindableButton extends Button implements IBindableView{
    private Scope mScope;
    private BindableAttrs mBindableAttrs;

    public BindableButton(Context context) {
        super(context);
        init(null);
    }

    public BindableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BindableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs){
        mBindableAttrs = new BindableAttrs(attrs);
    }

    @Override
    public void setNdModel(Object model) {
        BindableViewUtil.setNdText(this, model);
    }

    @Override
    public BindableAttrs getBindableAttrs() {
        return mBindableAttrs;
    }

    @Override
    public void onBind(Scope scope) {
        mScope = scope;
    }

    @Override
    public void onStartBinding(Scope scope) {

    }

    public void setNdText(Object text){
        BindableViewUtil.setNdText(this, text);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()){
            // ndClickに対応する
            case MotionEvent.ACTION_DOWN:
                mScope.notifyEvent(Scope.EVENT_CLICK, this);
                break;
        }

        return super.onTouchEvent(event);
    }
}
