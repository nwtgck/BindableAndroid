package jp.dip.nwtgck.bindable.evaluatable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Jimmy on 2015/04/01.
 */
public class EvaluatableMethod extends Evaluatable {
    private EvaluatableExpression mInstance;
    private String mMethodName;
    private List<EvaluatableExpression> mParams;

    public EvaluatableMethod(EvaluatableExpression instance, String methodName, List<EvaluatableExpression> params) {
        this.mInstance = instance;
        this.mMethodName = methodName;
        this.mParams = params;
    }

    @Override
    public Object eval() {
        /* 引数の作成 */
        // 評価後の引数
        final int size = mParams.size();
        Object[] evaledParams = new Object[size];
        Class[] paramClasses = new Class[size];
        for(int i = 0; i < size; i++){
            Object evaled = mParams.get(i).eval();
            // 評価して代入していく
            evaledParams[i] = evaled;
            // 引数の型も代入していく
            paramClasses[i] = evaled.getClass();
        }

        // インスタンスを評価する
        Object instance = mInstance.eval();

        // メソッドを探す
        try {
            Method method = instance.getClass().getMethod(mMethodName, paramClasses);
            return method.invoke(instance, evaledParams);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isChangable() {
        return true;
    }
}
