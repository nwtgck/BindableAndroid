package jp.dip.nwtgck.bindable.util;

import android.view.View;
import android.widget.TextView;

import jp.dip.nwtgck.bindable.iview.IEditableBindaleView;

/**
 * Created by Jimmy on 2015/03/30.
 *
 * BindableViewの定義を楽にするUtil
 */
public class BindableViewUtil {

    /**
     * イベントが発生してから評価する属性
     */
    private static final String[] eventAttrs = {
        "click"
    };


//    /**
//     * Bindableの属性からmodelNameの値をを取得する
//     * @param context
//     * @param attrs
//     * @return
//     */
//    public static String getModelNameFromAttr(Context context, AttributeSet attrs){
//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BindableView);
//        String modelName = a.getString(R.styleable.BindableView_model);
//        a.recycle();
//        return modelName;
//    }

    /**
     * viewを非表示にする
     * @param isHide
     */
    public static void setNdHide(View view, Object isHide){
        if(!(isHide instanceof Boolean)){
            throw new IllegalArgumentException("isHide must be boolean in setNdHide(isHide)!");
        }
        if((Boolean)isHide) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 文字をセットする
     * * EditTextもTextViewを継承している
     *
     * @param textView
     * @param text
     */
    public static void setNdText(TextView textView, Object text){
        textView.setText(text == null? "null": text.toString());
//        if(text instanceof CharSequence){
//            textView.setText((CharSequence)text);
//        } else {
//            throw new IllegalArgumentException("text must be instance of CharSequence in setText");
//        }
    }


    /**
     * モデル名を取得
     * @param editableBindaleView
     * @return
     */
    public static String getModelName(IEditableBindaleView editableBindaleView){
        return editableBindaleView.getBindableAttrs().getValueString("model");
    }

    /**
     * 属性がイベント属性かどうか
     * @param attrName
     * @return
     */
    public static boolean isEventAttr(String attrName){
        for(String eventAttr: eventAttrs){
            if(eventAttr.equals(attrName)){
                return true;
            }
        }
        return false;
    }
}
