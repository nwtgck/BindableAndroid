package jp.dip.nwtgck.bindable.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.TextView;

import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.util.BindableViewUtil;
import jp.dip.nwtgck.bindable.iview.IBindableView;

/**
 * Created by Jimmy on 2015/03/28.
 */
public class BindableTextView extends TextView implements IBindableView {
    private Scope mScope;
    private Context mContext;
    private BindableAttrs mBindableAttrs;
    private String mModelName;

    public BindableTextView(Context context) {
        super(context);
        init(context, null, -1);
    }

    public BindableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, -1);
    }

    public BindableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs, defStyleAttr);

    }

    /**
     * 初期化
     * modelNameの取得など
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    private void init(Context context, AttributeSet attrs, int defStyleAttr){
        mBindableAttrs = new BindableAttrs(attrs);
        mContext = context;
//        mModelName = BindableViewUtil.getModelNameFromAttr(context, attrs);
    }

    @Override
    public void setNdModel(Object model) {
        setText(model.toString());
    }


    @Override
    public BindableAttrs getBindableAttrs() {
        return mBindableAttrs;
    }

    @Override
    public void onBind(Scope scope) {
        mScope = scope;
    }

    @Override
    public void onStartBinding(Scope scope) {

    }

    /**
     * 文字をセットする
     * @param text
     */
    public void setNdText(Object text){
        BindableViewUtil.setNdText(this, text);
    }

    /**
     * viewを非表示にする
     * @param isHide
     */
    public void setNdHide(Object isHide){
        BindableViewUtil.setNdHide(this, isHide);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // ndChangeに対応させる
        mScope.notifyEvent(Scope.EVENT_CLICK, this);

        return super.onTouchEvent(event);
    }
}
