package jp.dip.nwtgck.bindable.evaluatable;

import jp.dip.nwtgck.bindable.component.Scope;

/**
 * Created by Jimmy on 2015/04/01.
 */
public class EvaluatableVariable extends Evaluatable {
    private Scope mScope;
    private String mVarName;

    // デバッグ用
    private Object debugEvaledValue;

    public EvaluatableVariable(Scope scope, String varName) {
        mScope = scope;
        mVarName = varName;

        try {
            // デバッグのために、評価した値をフィールドに
            debugEvaledValue = mScope.getValueByFieldName(mVarName);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
    }

    @Override
    public Object eval() {
        try {
            return mScope.getValueByFieldName(mVarName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return String.format("[%s not found]", mVarName);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return String.format("[%s not found]", mVarName);
        }
    }

    @Override
    public boolean isChangable() {
        return false;
    }
}
