package jp.dip.nwtgck.bindable.component;

import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Jimmy on 2015/04/03.
 *
 * 配列やListのラッパークラス
 * 配列もListも同じメソッドを使って処理できるのが特徴
 */
public class Arrangement<T extends Serializable> implements Serializable{
    // Arrangementの名前
    private String mName;
    // 配列の時に格納される
    private T[] mArray;
    // Listの時に格納される
    private ArrayList<T> mList;
    // 長さ
    private int mSize;
    // 配列かリストか
    private Type mType;

    // コンストラクタをとした時のハッシュ値
    private int initHash;

    // 元になった配列やリストの名前
    @Nullable private String mListName;

    // 種類を宣言（増える可能性がある）
    private static enum Type implements Serializable{
        ARRAY, LIST
    }

    /**
     * 配列から作成
     * @param array
     */
    public Arrangement(String name, T[] array, @Nullable String listName){
        mName = name;
        mArray = array;
        mSize = array.length;
        mType = Type.ARRAY;
        mListName = listName;

        initHash = mName.hashCode() ^ Arrays.hashCode(mArray);
    }

    /**
     * リストからの作成
     * @param list
     */
    public Arrangement(String name, ArrayList list, @Nullable String listName){
        mName = name;
        mList = list;
        mSize = list.size();
        mType = Type.LIST;
        mListName = listName;

        initHash = mName.hashCode() ^ mList.hashCode();
    }

//    /**
//     * 変更が反映されたArrangementを取得
//     * @return
//     */
//    public Arrangement<T> getUpdatedArrangement(){
//        if(mType.equals(Type.ARRAY)){
//            return new Arrangement<T>(mName, mArray);
//        } else {
//            return new Arrangement<T>(mName, mList);
//        }
//    }

    public Object getNowValue(){
        if(mType.equals(Type.ARRAY)){
            return mArray;
        } else {
            return mList;
        }
    }

    @Override
    public boolean equals(Object o) {
        // TODO [実装変更の必要] 同じオブジェクトであればハッシュ値は同じだが、違うオブジェクトでもハッシュ値が同じ場合はある
        return o instanceof Arrangement && hashCode() == o.hashCode();

//        if(o instanceof Arrangement){
//            Arrangement that = (Arrangement)o;
//            if(this.mType.equals(that.mType)){
//                if(mType.equals(Type.ARRAY)){
//                    return Arrays.equals(this.mArray, that.mArray);
////                    return false;
//                } else return mType.equals(Type.LIST) && (mList == null? that.mList == null: this.mList.equals(that.mList) );
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
    }

    @Override
    public int hashCode() {
        return initHash;
    }

    /**
     * 要素を指定して取得
     * @param index
     * @return
     */
    public Object get(int index){
        int nowSize = mType.equals(Type.ARRAY)? mArray.length: mList.size();
        if(index < 0 || index >= nowSize){
            return null;
        } else if(mType.equals(Type.ARRAY)){ // 配列なら
            return mArray[index];
//            return null;
        } else if(mType.equals(Type.LIST)){ // Listなら
            return mList.get(index);
        } else {
            return null; // 配列かListでない場合（ありえない）
        }
    }

    /**
     * 要素を指定して変更
     * @param index
     * @param newValue
     */
    public void set(int index, T newValue){
        if(index < 0 || index >= mSize){ // 範囲外ならエラー
            throw new ArrayIndexOutOfBoundsException("index of "+ index);
        } else if(mType.equals(Type.ARRAY)){ // 配列なら
            mArray[index] = newValue;
        } else if(mType.equals(Type.LIST)){ // Listなら
            mList.set(index, newValue);
        }
    }

    /**
     * 要素数
     * @return
     */
    public int size(){
        return mSize;
    }

    /**
     * 名前の取得
     * @return
     */
    public String getName() {
        return mName;
    }

    /**
     * 元になった配列やリストの名前を返す
     * @return
     */
    @Nullable public String getListName(){
        return mListName;
    }
}
