package jp.dip.nwtgck.bindable.component;


import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import jp.dip.nwtgck.bindable.util.Util;

/**
 * Created by Jimmy on 2015/03/30.
 */
public class Expression2 {

    /**
     * 演算子
     * 上のものほど優先順位が高い
     */
    private final String[] mOperators = {
            Operator.OPE_COMMA, // メソッドないのコンマの結びつきが強いため、一番優先順位を高くする
            Operator.OPE_DOT,
            Operator.OPE_NOT,
            Operator.OPE_MULTI,
            Operator.OPE_DIVIDE,
            Operator.OPE_MOD,
            Operator.OPE_PLUS,
            Operator.OPE_MINUS,
            Operator.OPE_LESS,
            Operator.OPE_GRATER,
            Operator.OPE_LESS_EQUALS,
            Operator.OPE_GRATER_EQUALS,
            Operator.OPE_EQUALS,
            Operator.OPE_NOT_EQUALS,
            Operator.OPE_AND_AND,
            Operator.OPE_BAR_BAR,
    };

    // 式が所属しているスコープ
    private Scope mScope;
    // トークンに分けたもの
    private List<String> mTokens;
    // バインドしているモデルの名前
    private List<String> mBindingModelNames = new ArrayList<String>();
    // 式の評価可能な形式
    private Evaluatable0 mEvaluatable0;

    public Expression2(Scope scope, String expString){
        this(scope, new Tokenization(expString).getTokens());
    }

    /**
     * トークンから作成
     * @param tokens
     */
    private Expression2(Scope scope, List<String> tokens){
        mTokens = tokens;
        mScope = scope;
        mEvaluatable0 = parse();

    }

    /**
     * 構文解析をして、評価可能な形式に返す
     * （mBindingModelNamesも操作する）
     * @return
     */
    private Evaluatable0 parse(){
        // tokensがカッコで囲われていれば
        if(tokensIsWrappedBracket(mTokens)){
            return tokensToUncoverEvaluatable(mTokens);
        } else
        if(tokensIsInteger(mTokens)){
            final Number integer = tokensToInteger(mTokens);
            return new Evaluatable0() {
                @Override
                public Object eval() {
                    return integer;
                }
            };
        } else
        if(tokensIsFunction(mTokens)){
            // 関数の戻り値の評価可能な形式
            return tokensToFunctionReturnEvalatable(mTokens);
        } else
        if(tokensIsBoolean(mTokens)){
            final Boolean bool = tokensToBoolean(mTokens);
            return new Evaluatable0() {
                @Override
                public Object eval() {
                    return bool;
                }
            };
        } else
        if(tokensIsNull(mTokens)){
            return new Evaluatable0() {
                @Override
                public Object eval() {
                    return null;
                }
            };
        } else
        if(tokensIsVariable(mTokens)){
            // モデル名を追加する
            addBindingModelName(mTokens.get(0));
            return new Evaluatable0() {
                @Override
                public Object eval() {
                    return tokensToVariable(mTokens);
                }
            };
        } else
        if(tokensIsString(mTokens)){
            final String str = tokensToString(mTokens);
            return new Evaluatable0() {
                @Override
                public Object eval() {
                    return str;
                }
            };
        } else
        if(tokensIsFloat(mTokens)){
            final Number fl = tokensToFloat(mTokens);
            return new Evaluatable0() {
                @Override
                public Object eval() {
                    return fl;
                }
            };
        } else
            // トークンが空の時（!は単項演算子なので、空になる）
            if(mTokens.isEmpty()){
                return new Evaluatable0() {
                    @Override
                    public Object eval() {
                        return null;
                    }
                };
            }

        // 演算子を持っているの判定
        boolean hasOperator = false;
        // 一番低い優先順位
        int minPriority  =Integer.MAX_VALUE;
        // 一番低い優先順位の演算子がある場所
        int minIdx = -1;
        // トークンのサイズ
        final int tokensSize = mTokens.size();

        /* トークンの一番右から低い優先順位の演算子を探す */
        // 終わりのカッコの数を数える
        int endBracketCount = 0;
        for(int i = tokensSize - 1; i >= 0; i--){
            String token = mTokens.get(i);
            if(token.equals("(")){
                // 終わりのカッコを閉じる
                endBracketCount--;
            } else
            if(token.equals(")")){
                // 終わりのカッコが増える
                endBracketCount++;
            }

            // カッコないでなく、トークンが演算子なら
            if(endBracketCount == 0 && isOperator(token)){
                int priority = getPriority(token);
                if(priority < minPriority){
                    // 優先順位の低い演算子を更新
                    minPriority = priority;
                    minIdx = i;
                    hasOperator = true;
                }
            }
        }

        if(hasOperator) {
            // 演算子と左右に分けたトークン
            final String operator = mTokens.get(minIdx);
            List<String> leftTokens = new ArrayList<String>();
            List<String> rightTokens = new ArrayList<String>();
            // leftに詰める
            for (int i = 0; i < minIdx; i++) {
                leftTokens.add(mTokens.get(i));
            }
            // rightに詰める
            for (int i = minIdx + 1; i < tokensSize; i++) {
                rightTokens.add(mTokens.get(i));
            }

            // TODO テスト表示
            System.out.println(leftTokens + " " + mTokens.get(minIdx) + " " + rightTokens);

            // メンバ変数
            if(rightIsField(operator, rightTokens)){
                Expression2 leftExp = new Expression2(mScope, leftTokens);
                final Evaluatable0 left = leftExp.getEvalatable();
                // 右のトークンからメンバ変数の名前を取得
                final String fieldName = tokensToFieldName(rightTokens);

                // インスタンスと関係するモデル名を追加
                addBindingModelNames(leftExp.getBindingModelNames());

                // leftの中のfieldNameのメンバ変数の値を返すEvaluatableを返す
                return new Evaluatable0() {
                    @Override
                    public Object eval() {
                        try {
                            // 評価する
                            Object evaled = left.eval();
                            // 「left.fieldName」の形式でデータを取得
                            Field field = evaled.getClass().getDeclaredField(fieldName);
                            return field.get(evaled);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                            String msg = String.format("[field %s not found]", fieldName);
                            Log.e(Util.LOG_ERROR_TAG, msg);
                            return null;
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                            String msg = String.format("[field %s not found]", fieldName);
                            Log.e(Util.LOG_ERROR_TAG, msg);
                            return null;
                        }

                    };
                };
            } else if(rightIsMethod(operator, rightTokens)) {
                // メソッドを実行した結果の評価可能な形で返す
                return tokensToMethodReturnEvaluatable(leftTokens, rightTokens);
            }

            // 演算子の左と右で式を作成
            Expression2 leftExp = new Expression2(mScope, leftTokens);
            Expression2 rightExp = new Expression2(mScope, rightTokens);

            // 左と右のバンドしているモデル名をいれる
            addBindingModelNames(leftExp.getBindingModelNames());
            addBindingModelNames(rightExp.getBindingModelNames());

            // 式から評価可能な形式を取得
            final Evaluatable0 left = leftExp.getEvalatable();
            final Evaluatable0 right = rightExp.getEvalatable();

            // 演算をするEvaluatableを返す
            return new Evaluatable0() {
                @Override
                public Object eval() {
                    Object leftEvaled = left.eval();
                    Object rightEvaled = right.eval();
                    Object res = Operator.calc(leftEvaled, rightEvaled, operator);
                    return res;
                }
            };

        }



        return null;
    }

    /**
     * 式の評価可能なEvaluatableを返す
     * @return
     */
    public Evaluatable0 getEvalatable(){
        return mEvaluatable0;
    }

    /**
     * トークンが演算子かどうか
     * @param token
     * @return
     */
    private boolean isOperator(String token){
        for(int i = 0; i < mOperators.length; i++){
            if(token.equals(mOperators[i])){
                return true;
            }
        }
        return false;
    }

    /**
     * 演算子の優先順位を返す
     * @param operator
     * @return
     */
    private int getPriority(String operator){
        for(int i = 0; i < mOperators.length; i++){
            if(mOperators[i].equals(operator)){
                return -i;
            }
        }
        return 1;
    }

    /**
     * 半角英数かどうか
     * @param text
     * @return
     */
    private boolean isAlphaNum(String text){
        return Pattern.compile("^[\\w]+$").matcher(text).find();
    }

    /**
     * rightTokensがメンバ変数かどうか
     * @param operator
     * @param rightTokens
     * @return
     */
    private boolean rightIsField(String operator, List<String> rightTokens){
        // 演算子はドットで、tokensが一つだけで、半角英数なら
        return operator.equals(".") && rightTokens.size() == 1 && isAlphaNum(rightTokens.get(0));
    }


    /**
     * rightTokensがメソッドかどうか
     * @param operator
     * @param rightTokens
     * @return
     */
    private boolean rightIsMethod(String operator, List<String> rightTokens){
        return operator.equals(".") && tokensIsFunction(rightTokens);
    }

    /**
     * tokensが整数かどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsInteger(List<String> tokens) {
        return tokens.size() == 1 && Pattern.compile("^-?[\\d]+$").matcher(tokens.get(0)).find();
    }

    /**
     * tokensが論理値かどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsBoolean(List<String> tokens){
        if(tokens.size() == 1){
            String token = tokens.get(0);
            return token.equals("true") || token.equals("false");
        } else {
            return false;
        }
    }

    /**
     * tokensが浮動小数点かどうか（doubleも含む）
     * @param tokens
     * @return
     */
    private boolean tokensIsFloat(List<String> tokens){
        return tokens.size() == 1 && Pattern.compile("^[\\d]+\\.[\\d]+$").matcher(tokens.get(0)).find();
    }

    /**
     * tokensが文字列かどうか("hello", 'hello'のどちらも文字列とする)
     * @param tokens
     * @return
     */
    private boolean tokensIsString(List<String> tokens){
        return tokens.size() == 3 &&
                (tokens.get(0).equals("\"") && tokens.get(2).equals("\"") ||
                        tokens.get(0).equals("'") && tokens.get(2).equals("'"));
    }

    /**
     * tokensが変数かどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsVariable(List<String> tokens){
        return tokens.size() == 1 && isAlphaNum(tokens.get(0));
    }

    /**
     * tokens がnullかどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsNull(List<String> tokens){
        return tokens.size() == 1 && tokens.get(0).equals("null");
    }

    /**
     * トークンのリストが関数なら
     * @param tokens
     * @return
     */
    private boolean tokensIsFunction(List<String> tokens){
        final int size = tokens.size();
        if(size >= 3){
            String startToken = tokens.get(0);
            String nextToken = tokens.get(1);
            String endToken = tokens.get(size-1);
            // startTokenが関数名として適切で、nextTokenが始まりのカッコで、endTokenが終わりのカッコなら
            if(isAlphaNum(startToken) && nextToken.equals("(") && endToken.equals(")")){
                // 始まりのカッコの個数をカウント
                int startBracketCount = 1;
                for(int i = 2; i < size; i++){
                    // 始まりのカッコなら
                    if(tokens.get(i).equals("(")) {
                        // カウントを増やす
                        startBracketCount++;
                        // 終わりのカッコなら
                    } else if(tokens.get(i).equals(")")) {
                        startBracketCount--;
                        // カッコが閉じたら、
                        if(startBracketCount == 0){
                            // 閉じた場所が最後の要素ならtrue
                            return i + 1 == size;
                        }
                    }
                }
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * tokensを整数にする
     * @param tokens
     * @return
     */
    private Number tokensToInteger(List<String> tokens){
        Long value = Long.valueOf(tokens.get(0));
        // 大きさによってlongかintにする
        if(value > Integer.MAX_VALUE || value < Integer.MIN_VALUE){
            return value;
        } else {
            return Integer.valueOf(tokens.get(0));
        }
    }

    /**
     * tokensを論理値にする
     * @param tokens
     * @return
     */
    private boolean tokensToBoolean(List<String> tokens){
        return Boolean.valueOf(tokens.get(0));
    }


    /**
     * スコープから変数を取ってくる
     * @param tokens
     * @return
     */
    private Object tokensToVariable(List<String> tokens){
        String variableName = tokens.get(0);
        try {
//            Field field = mScope.getField(variableName);
//            return field.get(mScope);
            return mScope.getValueByFieldName(variableName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return String.format("[%s not found]", variableName);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return String.format("[%s not found]", variableName);
        }
    }

    /**
     * tokensを文字列にする
     * @param tokens
     * @return
     */
    private String tokensToString(List<String> tokens){
        return tokens.get(1);
    }

    /**
     * tokensを浮動小数点にする
     * @param tokens
     * @return
     */
    private Double tokensToFloat(List<String> tokens){
        return Double.valueOf(tokens.get(0));
    }

    /**
     * tokensから関数の戻り値を評価可能な形式にする
     * @return
     */
    private Evaluatable0 tokensToFunctionReturnEvalatable(final List<String> tokens){
        final List<Evaluatable0> params = functionTokensToParams(tokens);
        return new Evaluatable0() {
            @Override
            public Object eval() {
                /* 引数の作成 */
                // 評価後の引数
                final int paramSize = params.size();
                Object[] evaledParams = new Object[paramSize];
                Class[] paramTypes = new Class[paramSize];
                for(int i = 0; i < paramSize; i++){
                    // 評価する
                    Object evaled = params.get(i).eval();
                    evaledParams[i] = evaled;
                    // evaledParamからクラスを取得
                    paramTypes[i] = (evaled == null)? Void.class: evaled.getClass();
                }

                // 関数名を取得
                String funcName = tokens.get(0);
                try {
                    // スコープから関数を取得
                    Method fuc = mScope.getMethod(funcName, paramTypes);
                    // 引数をいれて、関数を呼ぶ
                    return fuc.invoke(mScope, evaledParams);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return String.format("[%s not invoked in scope]", funcName);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                    return String.format("[function internal error]");
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                    return String.format("[function internal error]");
                }
            }
        };
    }

    /**
     * tokensからメソッドの戻り値を評価可能な形式にする
     * @param leftTokens
     * @param rightTokens
     * @return
     */
    private Evaluatable0 tokensToMethodReturnEvaluatable(List<String> leftTokens, final List<String> rightTokens){
        Expression2 instanceExp = new Expression2(mScope, leftTokens);
        // インスタンスを評価可能にする
        final Evaluatable0 instanceEvaluatable0 = instanceExp.getEvalatable();

        // インスタンスに関係とバインドしているモデル名を加える
        addBindingModelNames(instanceExp.getBindingModelNames());

        // rightTokensから引数を評価可能な形式で取得
        final List<Evaluatable0> params = functionTokensToParams(rightTokens);

        return new Evaluatable0() {
            @Override
            public Object eval() {
                /* 引数の作成 */
                // 評価後の引数
                final int size = params.size();
                Object[] evaledParams = new Object[size];
                Class[] paramClasses = new Class[size];
                for(int i = 0; i < size; i++){
                    Object evaled = params.get(i).eval();
                    // 評価して代入していく
                    evaledParams[i] = evaled;
                    // 引数の型も代入していく
                    paramClasses[i] = evaled.getClass();
                }

                // メソッドの名前を取得
                String methodName = rightTokens.get(0);
                // インスタンスを評価する
                Object instance = instanceEvaluatable0.eval();

                // メソッドを探す
                try {
                    Method method = instance.getClass().getMethod(methodName, paramClasses);
                    return method.invoke(instance, evaledParams);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                return null;
            }
        };

    }

    /**
     * tokensから変数名を取得する
     * @param tokens
     * @return
     */
    private String tokensToFieldName(List<String> tokens){
        return tokens.get(0);
    }

    /**
     * Evaluatableになった引数のリスト。
     * 関数やメソッドのtokensをカンマ区切りでわけ、
     * Evaluatableのリストにして返す
     * @return
     */
    private List<Evaluatable0> functionTokensToParams(List<String> functionTokens){
        // function
        final int size = functionTokens.size();
        // 戻り値になる値
        List<Evaluatable0> params = new ArrayList<Evaluatable0>();
        // 引数の一塊のtokens
        List<String> paramTokens = new ArrayList<String>();
        for(int i = 2; i < size; i++){
            String token = functionTokens.get(i);
            // 引数に区切りがついたら
            if(token.equals(",") || i == size - 1){

                if(!paramTokens.isEmpty()) {
                    Expression2 paramExp = new Expression2(mScope, paramTokens);
                    // パラメータに関連したモデル名を加える
                    addBindingModelNames(paramExp.getBindingModelNames());
                    // 評価可能にして
                    Evaluatable0 evaluatable0 = paramExp.getEvalatable();
                    // 引数についかする
                    params.add(evaluatable0);
                    // 次の引数を入れていくために、新しいインスタンスにする
                    paramTokens = new ArrayList<String>();
                }
            } else {
                paramTokens.add(token);
            }
        }
        return params;
    }

    /**
     * tokensがカッコで囲われていれば
     * @param tokens
     * @return
     */
    private boolean tokensIsWrappedBracket(List<String> tokens) {
        final int size = tokens.size();
        if (size >= 2 && tokens.get(0).equals("(")) {
            int startBracketCount = 1;
            for(int i = 1; i < size; i++){
                if(tokens.get(i).equals("(")){
                    startBracketCount++;
                } else if(tokens.get(i).equals(")")){
                    startBracketCount--;
                    if(startBracketCount == 0){
                        return i == size -1;
                    }
                }
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * 外側のカッコを外したtokensからEvaluatableを作成
     * @param tokens
     * @return
     */
    private Evaluatable0 tokensToUncoverEvaluatable(List<String> tokens){
        List<String> uncoverTokens = new ArrayList<String>();
        final int size = tokens.size();
        for(int i = 1; i < size - 1; i++){
            uncoverTokens.add(tokens.get(i));
        }
        return new Expression2(mScope, uncoverTokens).getEvalatable();
    }

    /**
     * バインドしているモデルの名前を追加
     * @param modelName
     */
    private void addBindingModelName(String modelName){
        mBindingModelNames.add(modelName);
    }

    /**
     * バインドしている名前を追加
     * @param modelNames
     */
    private void addBindingModelNames(List<String> modelNames){
        mBindingModelNames.addAll(modelNames);
    }


    /**
     * バインドしているモデル名前を取得
     * @return
     */
    public List<String> getBindingModelNames(){
        return mBindingModelNames;
    }
}
