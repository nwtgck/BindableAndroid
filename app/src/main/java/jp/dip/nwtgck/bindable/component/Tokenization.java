package jp.dip.nwtgck.bindable.component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jimmy on 2015/03/30.
 */
public class Tokenization {
    String mText;
    List<String> mTokens = new ArrayList<String>();

    /**
     * 文字によっての終了判定を定義するためのクラス
     */
    abstract private static class IsEnd{
        private boolean nextEnd = false;
        protected static final int NOT_END = 0;
        protected static final int END_NOW = 1;
        protected static final int END_NEXT = 2;

        abstract int define(char prev, char c, char next);

        final boolean apply(char prev, char c, char next){
            if(nextEnd){
                return true;
            }

            switch(define(prev, c, next)){
                case NOT_END: return false;
                case END_NOW: return true;
                case END_NEXT:
                    nextEnd = true;
                    return false;
            }

            return false;

        }

    }

    public Tokenization(String text){
        mText = text;
        // トークンに分ける
        tokenize();
    }

    /**
     * トークンで分ける
     */
    private void tokenize(){
        // 空白なしにする
        String noBlankText = mText.replaceAll("\\s", "");
        int charIdx = 0;
        int length = noBlankText.length();
        while(charIdx < length){
            IsEnd isEnd = getIsEnd( noBlankText.charAt(charIdx) );
            String token = "";
            char now, prev, next;
            do {
                now = noBlankText.charAt(charIdx);
                prev = (charIdx == 0)? '\0': noBlankText.charAt(charIdx-1);
                next = (charIdx == length-1)? '\0': noBlankText.charAt(charIdx+1);
                token += now;
            } while (++charIdx < length && !isEnd.apply(prev, now, next));
            mTokens.add(token);
        }
    }

    /**
     * 分割したトークンを取得
     * @return
     */
    public List<String> getTokens(){
        return mTokens;
    }

    /**
     * 始まりの文字を与えて、どういう時に終了するかを返すisEndを返す
     * @param ch
     * @return
     */
    private static IsEnd getIsEnd(final char ch){
        // すぐに終了にするとき
        if(ch == '(' || ch == ')' || ch == '.' || ch == ',' || ch == ':'){
            return  new IsEnd() {
                @Override
                int define(char prev, char c, char next) {
                    return END_NOW;
                }
            };
        } else
        // イコールまでつづく
        if(ch == '=' | ch == '+' | ch == '-'| ch == '*' | ch == '/' | ch == '<' || ch == '>' || ch == '!' || ch == '%'){
            return new IsEnd() {
                @Override
                int define(char prev, char c, char next) {
                    return next == '='? END_NEXT: END_NOW;
                }
            };
        } else
        if(ch == '\"' || ch == '\''){
            return new IsEnd() {
                int endCount = 3;

                @Override
                int define(char prev, char now, char next) {
                    if(now == ch || next == ch) endCount--;
                    return endCount == 0? END_NOW: NOT_END;
                }
            };
        } else
//        if(ch == '-'){
//            return new IsEnd() {
//                @Override
//                int define(char prev, char c, char next) {
//                    // prevが数字なら
//                    if('0' <= prev && prev <= '9') {
//                        return END_NOW;
//                    } else if('0' <= next && next <= '9' || 'a' <= next && next <= 'z' || 'A' <= next && next <= 'Z'){
//                        return NOT_END;
//                    } else {
//                        return END_NOW;
//                    }
//                }
//            };
//        } else
        // アルファベットのとき アルファベットか数字まで続く
        if('a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'){
            return new IsEnd() {
                @Override
                int define(char prev, char c, char next) {
                    return 'a' <= next && next <= 'z' || 'A' <= next && next <= 'Z' || '0' <= next && next <= '9' || next == '_'?
                            NOT_END: END_NOW;
                }
            };
        } else
        // 数字の時　数字か.まで続く
        if('0' <= ch && ch <= '9'){
            return new IsEnd() {
                @Override
                int define(char prev, char c, char next) {
                    return '0' <= next && next <= '9' || next == '.'? NOT_END: END_NOW;
                }
            };
        } else
        // |のとき
        if(ch == '|'){
            return new IsEnd() {
                @Override
                int define(char prev, char c, char next) {
                    return next == '|'? END_NEXT: END_NOW;
                }
            };
        } else
        // &のとき
        if(ch == '&'){
            return new IsEnd() {
                @Override
                int define(char prev, char c, char next) {
                    return next == '&'? END_NEXT: END_NOW;
                }
            };
        }


        throw new IllegalArgumentException("「"+ch+"」 は想定外の文字です");
    }

    @Override
    public String toString() {
        return mTokens.toString();
    }
}
