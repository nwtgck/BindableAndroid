package jp.dip.nwtgck.bindable.view;

import android.content.Context;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.util.BindableViewUtil;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.iview.IEditableBindaleView;

/**
 * Created by Jimmy on 2015/03/29.
 */
public class BindableEditText extends EditText implements IEditableBindaleView {
    private BindableAttrs mBindableAttrs;

    // setModelでセット後にgetNdModel()した値を補完
    private Object mSetOldModel;

    public BindableEditText(Context context) {
        super(context);
        init(context, null, -1);
    }

    public BindableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, -1);
    }

    public BindableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    /**
     * 初期化
     * modelNameの取得など
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    private void init(Context context, AttributeSet attrs, int defStyleAttr){
        mBindableAttrs = new BindableAttrs(attrs);
    }


    @Override
    public void setNdModel(Object model) {
        setText(model.toString());
        mSetOldModel = getNdModel();
    }

    @Override
    public Object getNdModel() {
        return ((SpannableStringBuilder)getText()).toString();
    }

    @Override
    public <S extends Scope> void defModelChanged(final S scope) {
        // キーが押された時に変更を伝える
        addTextChangedListener(new TextWatcher() {
            // 古いテキスト保管しておく
            String oldText = getText().toString();

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                scope.notifyModelChanged(BindableEditText.this);
            }
        });
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);

    }

    @Override
    public BindableAttrs getBindableAttrs() {
        return mBindableAttrs;
    }

    @Override
    public void onBind(Scope scope) {

    }

    @Override
    public void onStartBinding(Scope scope) {

    }

    /**
     * 文字をセットする
     * @param text
     */
    public void setNdText(Object text){
        BindableViewUtil.setNdText(this, text);
    }

    /**
     * viewを非表示にする
     * @param isHide
     */
    public void setNdHide(Object isHide){
        BindableViewUtil.setNdHide(this, isHide);
    }
}
