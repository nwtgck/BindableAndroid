package jp.dip.nwtgck.bindable.evaluatable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import jp.dip.nwtgck.bindable.component.Scope;

/**
 * Created by Jimmy on 2015/04/01.
 *
 * 評価可能な複数の式の集まり
 * 関数などに利用
 */
public class EvaluatableFunction extends Evaluatable {
    private Scope mScope;
    private String mFuncName;
    private List<EvaluatableExpression> mParamExps;
    private Method mMethod;

    public EvaluatableFunction(Scope scope, String funcName, List<EvaluatableExpression> paramsExps){
        mScope = scope;
        mFuncName = funcName;
        mParamExps = paramsExps;
    }

    @Override
    public Object eval() {
        /* 引数の作成 */
        // 評価後の引数
        final int paramSize = mParamExps.size();
        Object[] evaledParams = new Object[paramSize];
        Class[] paramTypes = new Class[paramSize];
        for(int i = 0; i < paramSize; i++){
            // 評価する
            Object evaled = mParamExps.get(i).eval();
            evaledParams[i] = evaled;
            // evaledParamからクラスを取得
            paramTypes[i] = (evaled == null)? Void.class: evaled.getClass();
        }

        try {
            // スコープから関数を取得（一度取得したらとっておく）
            if (mMethod == null){
               mMethod = mScope.getMethod(mFuncName, paramTypes);
            }
            // 引数をいれて、関数を呼ぶ
            return mMethod.invoke(mScope, evaledParams);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return String.format("[%s not invoked in scope]", mFuncName);
        } catch (NoSuchMethodException e) {
            // 親スコープを取得
            Scope parentScope = mScope.getParentScope();
            // 親スコープが存在すれば、
            if(parentScope != null){
                EvaluatableFunction parentFunc = new EvaluatableFunction(parentScope, mFuncName, mParamExps);
                return parentFunc.eval();
//                try {
//                    // 親スコープから関数を取得してみる
//                    Method parentFuc=  parentScope.getMethod(mFuncName, paramTypes);
//                    // 引数をいれて、関数を呼ぶ
//                    return parentFuc.invoke(parentScope, evaledParams);
//                } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e1) {
//                    e1.printStackTrace();
//                }
            }
            return String.format("[function internal error]");
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return String.format("[function internal error]");
        }
    }

    @Override
    public boolean isChangable() {
        return true;
    }
}
