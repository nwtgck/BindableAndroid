package jp.dip.nwtgck.bindable.evaluatable;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import jp.dip.nwtgck.bindable.component.Operator;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.component.Tokenization;

/**
 * Created by Jimmy on 2015/03/30.
 */
public class EvaluatableExpression extends Evaluatable {
    private Evaluatable mSelf;

    /**
     * 演算子
     * 上のものほど優先順位が高い
     */
    private final String[] mOperators = {
            Operator.OPE_COMMA, // メソッドないのコンマの結びつきが強いため、一番優先順位を高くする
            ARRANGE_OPERATOR, // Arrangementを作る「:」
            Operator.OPE_DOT,
            Operator.OPE_NOT,
            UNIT_OPERATOR, // 単項演算子の優先順位
            Operator.OPE_MULTI,
            Operator.OPE_DIVIDE,
            Operator.OPE_MOD,
            Operator.OPE_PLUS,
            Operator.OPE_MINUS,
            Operator.OPE_LESS,
            Operator.OPE_GRATER,
            Operator.OPE_LESS_EQUALS,
            Operator.OPE_GRATER_EQUALS,
            Operator.OPE_EQUALS,
            Operator.OPE_NOT_EQUALS,
            Operator.OPE_AND_AND,
            Operator.OPE_BAR_BAR,
    };

    // 式が所属しているスコープ
    private Scope mScope;
    // トークンに分けたもの
    private List<String> mTokens;
    // バインドしているモデルの名前
    private Set<String> mBindingFieldNames = new HashSet<>();

    // 単項演算子の優先順位ために
    private static final String UNIT_OPERATOR = "UNIT_OPERATOR";

    // Arrangementを作る演算子
    private static final String ARRANGE_OPERATOR = ":";

    public EvaluatableExpression(Scope scope, String expString){
        this(scope, new Tokenization(expString).getTokens());
    }

    /**
     * トークンから作成
     * @param tokens
     */
    private EvaluatableExpression(Scope scope, List<String> tokens){
        mTokens = tokens;
        mScope = scope;
        mSelf = parse();
    }

    /* トークンの分け方が等しければ等しい */
    @Override
    public int hashCode() {
        return mTokens.hashCode();
    }

    /* トークンの分け方が等しければ等しい */
    @Override
    public boolean equals(Object o) {
        return (o instanceof EvaluatableExpression) && mTokens.equals(((EvaluatableExpression) o).mTokens);
    }

    /**
     * 構文解析して、
     * （mBindingModelNamesも操作する）
     * @return
     */
    private Evaluatable parse(){
        // tokensがカッコで囲われていれば
        if(tokensIsWrappedBracket(mTokens)){
            EvaluatableExpression uncoverExp = tokensToUncoverExpression(mTokens);
            addBindingModelNames(uncoverExp.getBindingFieldNames());
            return uncoverExp;
        } else
        if(tokensIsInteger(mTokens)){
            final Number integer = tokensToInteger(mTokens);
            return new EvaluatableConstant(integer);
        } else
        if(tokensIsFunction(mTokens)){
            // 関数の引数を式の配列とみる
            List<EvaluatableExpression> paramExpressions =  functionTokensToExpressions(mTokens);
            String funcName = mTokens.get(0);
            return new EvaluatableFunction(mScope, funcName, paramExpressions);
        } else
        if(tokensIsBoolean(mTokens)){
            final Boolean bool = tokensToBoolean(mTokens);
            return new EvaluatableConstant(bool);
        } else
        if(tokensIsNull(mTokens)){
            return new EvaluatableConstant(null);
        } else
        if(tokensIsVariable(mTokens)){
            // モデル名を追加する
            addBindingModelName(mTokens.get(0));
            String varName = mTokens.get(0);

            return new EvaluatableVariable(mScope, varName);
        } else
        if(tokensIsString(mTokens)){
            final String str = tokensToString(mTokens);
            return new EvaluatableConstant(str);
        } else
        if(tokensIsFloat(mTokens)){
            final Number fl = tokensToFloat(mTokens);
            return new EvaluatableConstant(fl);
        } else
        // トークンが空の時（!は単項演算子なので、空になる）
        if(mTokens.isEmpty()){
            return new EvaluatableConstant(null);
        }

        // 演算子を持っているの判定
        boolean hasOperator = false;
        // 一番低い優先順位
        int minPriority  =Integer.MAX_VALUE;
        // 一番低い優先順位の演算子がある場所
        int minIdx = -1;
        // トークンのサイズ
        final int tokensSize = mTokens.size();

        /* トークンの一番右から低い優先順位の演算子を探す */
        // 終わりのカッコの数を数える
        int endBracketCount = 0;
        for(int i = tokensSize - 1; i >= 0; i--){
            String token = mTokens.get(i);
            if(token.equals("(")){
                // 終わりのカッコを閉じる
                endBracketCount--;
            } else
            if(token.equals(")")){
                // 終わりのカッコが増える
                endBracketCount++;
            }

            // カッコ内でなく、トークンが演算子なら
            if(endBracketCount == 0 && isOperator(token)){
                // 単項演算子の時の優先順位
                String operator = token.equals("-") && (i == 0 || isOperator(mTokens.get(i-1)) )?
                        UNIT_OPERATOR: token;

                int priority = getPriority(operator);
                if(priority < minPriority){
                    // 優先順位の低い演算子を更新
                    minPriority = priority;
                    minIdx = i;
                    hasOperator = true;
                }
            }
        }

        if(hasOperator) {
            // 演算子と左右に分けたトークン
            final String operator = mTokens.get(minIdx);
            List<String> leftTokens = new ArrayList<String>();
            List<String> rightTokens = new ArrayList<String>();
            // leftに詰める
            for (int i = 0; i < minIdx; i++) {
                leftTokens.add(mTokens.get(i));
            }
            // rightに詰める
            for (int i = minIdx + 1; i < tokensSize; i++) {
                rightTokens.add(mTokens.get(i));
            }

            // 負の数なら
            if(leftTokens.isEmpty() && operator.equals("-")){
                EvaluatableExpression rightExp = new EvaluatableExpression(mScope, rightTokens);
                return new EvaluatableNegative(rightExp);
            }

            // TODO テスト表示
            System.out.println(leftTokens + " " + mTokens.get(minIdx) + " " + rightTokens);

            // メンバ変数
            if(rightIsField(operator, rightTokens)){
                EvaluatableExpression leftExp = new EvaluatableExpression(mScope, leftTokens);

                // 右のトークンからメンバ変数の名前を取得
                final String fieldName = tokensToFieldName(rightTokens);

                // インスタンスと関係するモデル名を追加
                addBindingModelNames(leftExp.getBindingFieldNames());

                return new EvaluatableField(leftExp, fieldName);
            } else if(rightIsMethod(operator, rightTokens)) {
                // メソッドを実行した結果の評価可能な形で返す
                EvaluatableExpression instanceExp = new EvaluatableExpression(mScope, leftTokens);

                // インスタンスに関係とバインドしているモデル名を加える
                addBindingModelNames(instanceExp.getBindingFieldNames());

                // rightTokensから引数を評価可能な形式で取得
                final List<EvaluatableExpression> params = functionTokensToExpressions(rightTokens);


                // メソッドの名前を取得
                final String methodName = rightTokens.get(0);

                return new EvaluatableMethod(instanceExp, methodName, params);

            // Arrangementなら
            } else if(operator.equals(ARRANGE_OPERATOR)){
                EvaluatableExpression arrangementExp = new EvaluatableExpression(mScope, rightTokens);
                String arrangementName = leftTokens.get(0);
                // 関連しているフィールド名を取得
                addBindingModelNames(arrangementExp.getBindingFieldNames());
                return new EvaluatableArrangement(mScope, arrangementName, arrangementExp);
            }

            // 演算子の左と右で式を作成
            EvaluatableExpression leftExp = new EvaluatableExpression(mScope, leftTokens);
            EvaluatableExpression rightExp = new EvaluatableExpression(mScope, rightTokens);

            // 左と右のバンドしているモデル名をいれる
            addBindingModelNames(leftExp.getBindingFieldNames());
            addBindingModelNames(rightExp.getBindingFieldNames());

            return new EvaluatableBiominal(leftExp, rightExp, operator);

        }

        return null;
    }

    /**
     * トークンが演算子かどうか
     * @param token
     * @return
     */
    private boolean isOperator(String token){
        for(int i = 0; i < mOperators.length; i++){
            if(token.equals(mOperators[i])){
                return true;
            }
        }
        return false;
    }

    /**
     * 演算子の優先順位を返す
     * @param operator
     * @return
     */
    private int getPriority(String operator){
        for(int i = 0; i < mOperators.length; i++){
            if(mOperators[i].equals(operator)){
                return -i;
            }
        }
        return 1;
    }

    /**
     * 半角英数かどうか
     * @param text
     * @return
     */
    private boolean isAlphaNum(String text){
        return Pattern.compile("^[\\w]+$").matcher(text).find();
    }

    /**
     * rightTokensがメンバ変数かどうか
     * @param operator
     * @param rightTokens
     * @return
     */
    private boolean rightIsField(String operator, List<String> rightTokens){
        // 演算子はドットで、tokensが一つだけで、半角英数なら
        return operator.equals(".") && rightTokens.size() == 1 && isAlphaNum(rightTokens.get(0));
    }


    /**
     * rightTokensがメソッドかどうか
     * @param operator
     * @param rightTokens
     * @return
     */
    private boolean rightIsMethod(String operator, List<String> rightTokens){
        return operator.equals(".") && tokensIsFunction(rightTokens);
    }

    /**
     * tokensが整数かどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsInteger(List<String> tokens) {
        return tokens.size() == 1 && Pattern.compile("^-?[\\d]+$").matcher(tokens.get(0)).find();
    }

    /**
     * tokensが論理値かどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsBoolean(List<String> tokens){
        if(tokens.size() == 1){
            String token = tokens.get(0);
            return token.equals("true") || token.equals("false");
        } else {
            return false;
        }
    }

    /**
     * tokensが浮動小数点かどうか（doubleも含む）
     * @param tokens
     * @return
     */
    private boolean tokensIsFloat(List<String> tokens){
        return tokens.size() == 1 && Pattern.compile("^[\\d]+\\.[\\d]+$").matcher(tokens.get(0)).find();
    }

    /**
     * tokensが文字列かどうか("hello", 'hello'のどちらも文字列とする)
     * @param tokens
     * @return
     */
    private boolean tokensIsString(List<String> tokens){
        if(tokens.size() == 1){
            String token = tokens.get(0);
            if(token.length() >= 2){
                char start = token.charAt(0);
                char end = token.charAt(token.length()-1);
                return start == '\'' && end == '\'' || start == '\"' && end == '\"';
            } else {
                return false;
            }
        } else {
            return false;
        }
//        return tokens.size() == 1 &&
//                (tokens.get(0).equals("\"") && tokens.get(2).equals("\"") ||
//                        tokens.get(0).equals("'") && tokens.get(2).equals("'"));
    }

    /**
     * tokensが変数かどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsVariable(List<String> tokens){
        return tokens.size() == 1 && isAlphaNum(tokens.get(0));
    }

    /**
     * tokens がnullかどうか
     * @param tokens
     * @return
     */
    private boolean tokensIsNull(List<String> tokens){
        return tokens.size() == 1 && tokens.get(0).equals("null");
    }

    /**
     * トークンのリストが関数なら
     * @param tokens
     * @return
     */
    private boolean tokensIsFunction(List<String> tokens){
        final int size = tokens.size();
        if(size >= 3){
            String startToken = tokens.get(0);
            String nextToken = tokens.get(1);
            String endToken = tokens.get(size-1);
            // startTokenが関数名として適切で、nextTokenが始まりのカッコで、endTokenが終わりのカッコなら
            if(isAlphaNum(startToken) && nextToken.equals("(") && endToken.equals(")")){
                // 始まりのカッコの個数をカウント
                int startBracketCount = 1;
                for(int i = 2; i < size; i++){
                    // 始まりのカッコなら
                    if(tokens.get(i).equals("(")) {
                        // カウントを増やす
                        startBracketCount++;
                        // 終わりのカッコなら
                    } else if(tokens.get(i).equals(")")) {
                        startBracketCount--;
                        // カッコが閉じたら、
                        if(startBracketCount == 0){
                            // 閉じた場所が最後の要素ならtrue
                            return i + 1 == size;
                        }
                    }
                }
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * tokensを整数にする
     * @param tokens
     * @return
     */
    private Number tokensToInteger(List<String> tokens){
        Long value = Long.valueOf(tokens.get(0));
        // 大きさによってlongかintにする
        if(value > Integer.MAX_VALUE || value < Integer.MIN_VALUE){
            return value;
        } else {
            return Integer.valueOf(tokens.get(0));
        }
    }

    /**
     * tokensを論理値にする
     * @param tokens
     * @return
     */
    private boolean tokensToBoolean(List<String> tokens){
        return Boolean.valueOf(tokens.get(0));
    }

    /**
     * tokensを文字列にする
     * @param tokens
     * @return
     */
    private String tokensToString(List<String> tokens){
        String withQuate = tokens.get(0);
        return withQuate.substring(1, withQuate.length()-1);
    }

    /**
     * tokensを浮動小数点にする
     * @param tokens
     * @return
     */
    private Double tokensToFloat(List<String> tokens){
        return Double.valueOf(tokens.get(0));
    }

    private List<EvaluatableExpression> functionTokensToExpressions(List<String> functionTokens){
        // function
        final int size = functionTokens.size();
        // 戻り値になる値
        List<EvaluatableExpression> params = new ArrayList<EvaluatableExpression>();
        // 引数の一塊のtokens
        List<String> paramTokens = new ArrayList<String>();
        for(int i = 2; i < size; i++){
            String token = functionTokens.get(i);
            // 引数に区切りがついたら
            if(token.equals(",") || i == size - 1){

                if(!paramTokens.isEmpty()) {
                    EvaluatableExpression paramExp = new EvaluatableExpression(mScope, paramTokens);
                    // パラメータに関連したモデル名を加える
                    addBindingModelNames(paramExp.getBindingFieldNames());
                    // 引数についかする
                    params.add(paramExp);
                    // 次の引数を入れていくために、新しいインスタンスにする
                    paramTokens = new ArrayList<String>();
                }
            } else {
                paramTokens.add(token);
            }
        }
        return params;
    }

    /**
     * tokensから変数名を取得する
     * @param tokens
     * @return
     */
    private String tokensToFieldName(List<String> tokens){
        return tokens.get(0);
    }

    /**
     * tokensがカッコで囲われていれば
     * @param tokens
     * @return
     */
    private boolean tokensIsWrappedBracket(List<String> tokens) {
        final int size = tokens.size();
        if (size >= 2 && tokens.get(0).equals("(") && tokens.get(size-1).equals(")")) {
            int startBracketCount = 1;
            for(int i = 1; i < size; i++){
                if(tokens.get(i).equals("(")){
                    startBracketCount++;
                } else if(tokens.get(i).equals(")")){
                    startBracketCount--;
                    if(startBracketCount == 0){
                        return i == size -1;
                    }
                }
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * 外側のカッコを外したtokensからEvaluatableを作成
     * @param tokens
     * @return
     */
    private EvaluatableExpression tokensToUncoverExpression(List<String> tokens){
        List<String> uncoverTokens = new ArrayList<String>();
        final int size = tokens.size();
        for(int i = 1; i < size - 1; i++){
            uncoverTokens.add(tokens.get(i));
        }
        return new EvaluatableExpression(mScope, uncoverTokens);
    }

    /**
     * バインドしているモデルの名前を追加
     * @param modelName
     */
    private void addBindingModelName(String modelName){
        mBindingFieldNames.add(modelName);
    }

    /**
     * バインドしている名前を追加
     * @param modelNames
     */
    private void addBindingModelNames(Set<String> modelNames){
        mBindingFieldNames.addAll(modelNames);
    }


    /**
     * バインドしているモデル名前を取得
     * @return
     */
    public Set<String> getBindingFieldNames(){
        return mBindingFieldNames;
    }

    @Override
    public Object eval() {
        return mSelf.eval();
    }

    @Override
    public boolean isChangable() {
        return mSelf.isChangable();
    }

    /**
     * 式の内容を返す
     *
     * @return
     */
    public Evaluatable getSelf(){
        return mSelf;
    }
}
