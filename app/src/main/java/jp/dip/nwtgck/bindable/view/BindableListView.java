package jp.dip.nwtgck.bindable.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ListView;

import jp.dip.nwtgck.bindable.adapter.BindableListViewAdapter;
import jp.dip.nwtgck.bindable.component.Arrangement;
import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.iview.IBindableView;

/**
 * Created by Jimmy on 2015/04/03.
 */
public class BindableListView extends ListView implements IBindableView {
    private LayoutInflater mLayoutInflater;
    private BindableAttrs mBindableAttrs;
    private Scope mParentScope;
    private BindableListViewAdapter mAdapter;
    private int mItemId;

    public BindableListView(Context context) {
        super(context);
        init(context, null);
    }

    public BindableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BindableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * 初期化
     * @param context
     * @param attrs
     */
    private void init(Context context, AttributeSet attrs){
        mBindableAttrs = new BindableAttrs(attrs);
        mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void setNdModel(Object model) {
        if(model instanceof Arrangement){

            Arrangement arrangement = (Arrangement)model;

            // アダプターを未設定のとき
            if(mAdapter == null) {
                // アダプターを作成する
                mAdapter = new BindableListViewAdapter(mLayoutInflater, mItemId, mParentScope, arrangement);
                this.setAdapter(mAdapter);
            } else {
                // アダプターでデータを更新する
                mAdapter.updateData(arrangement);
            }

        } else {
            throw new IllegalArgumentException("model must be instance of boolean in Arrangement");
        }
    }

    /**
     * リストの項目のIDを指定
     * @param itemId
     */
    public void setNdItemLayout(Object itemId){
        if(itemId instanceof Integer){
            mItemId = (int)itemId;
        } else {
            throw new IllegalArgumentException("itemId must be int");
        }
    }

    @Override
    public BindableAttrs getBindableAttrs() {
        return mBindableAttrs;
    }

    @Override
    public void onBind(Scope scope) {
    }

    @Override
    public void onStartBinding(Scope scope) {
        mParentScope = scope;
    }
}
