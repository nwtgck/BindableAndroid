package jp.dip.nwtgck.bindable.util;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.evaluatable.EvaluatableExpression;
import jp.dip.nwtgck.bindable.iview.IBindableView;
import jp.dip.nwtgck.bindable.iview.IEditableBindaleView;

/**
 * Created by Jimmy on 2015/03/28.
 */
public class Util {

    public static final String LOG_ERROR_TAG = "error:BindingAndroid";
    public static final String LOG_WARNING_TAG = "warning:BindingAndroid";

    // Viewの属性につけるプレフィックス(nd_hideなど)
    public static final String ND_ATTR_PREFIX = "nd_";
    public static final String ND_METHOD_NAME = "Nd";

    /**
     * @param activity
     * @return contentView
     */
    public static ViewGroup getContentView(Activity activity){
        ViewGroup root = (ViewGroup)activity.getWindow().getDecorView().findViewById(android.R.id.content);
        ViewGroup contentView = (ViewGroup)root.getChildAt(0);
        return contentView;
    }

    /**
     * レイアウトのViewとscopeの値を結びつける
     * @param activity
     * @param layout
     * @param scope
     */
    public static void bindActivityModelsAndViews(Activity activity, int layout, Scope scope){
        // レイアウトをセット
        activity.setContentView(layout);

        // contentViewを取得
        ViewGroup contentView = Util.getContentView(activity);

        // 再帰的に子要素のviewをバインドする
        bindViewsAndModels(contentView, scope, true);

        // スコープにバインド終了したことを伝える
        scope.onEndBindActivity();
    }

    public static void bindViewsAndModels(View parentView, Scope scope, boolean enrollScope){
        // parentViewはViewGroupであり、子要素が存在するなら
        if(parentView instanceof ViewGroup && ((ViewGroup) parentView).getChildCount() != 0) {

            ViewGroup parent = (ViewGroup)parentView;

            // contentViewの子の数を取得
            int childCount = parent.getChildCount();

            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                // 再帰的にparentの子要素もバンドさせる
                bindViewsAndModels(child, scope, enrollScope);

                // 親のなかにあるviewがIBindableViewなら、
                if (child instanceof IBindableView) {
                    IBindableView bindableView = (IBindableView) child;

                    // バインドのスタートを知らせる
                    bindableView.onStartBinding(scope);

                    // バインドする属性を取得
                    BindableAttrs bindableAttrs = bindableView.getBindableAttrs();
                    
                    for (String attrName : bindableAttrs.getAttrNames()) {

                        // イベント属性なら、評価しないようにする
                        if(BindableViewUtil.isEventAttr(attrName)){
                            continue;
                        }

                        // 属性の値を文字列で取得
                        String expString = bindableAttrs.getValueString(attrName);
                        // 文字列の式を評価するオブジェクト作成
                        EvaluatableExpression expression = new EvaluatableExpression(scope, expString);
//                        // 式を評価可能にしたものを取得
//                        Expression3 evaluatable = expression.getEvalatable();
                        // 評価した値
                        Object value = expression.eval();
                        // バインドしているモデル名を取得

//                        boolean b = expression.isChangable();
//                        int mm = 0;

                        try {
                            // 属性の値をvalueの値で初期化する
                            Method updateAttrMethod = Util.getUpdateAttrMethod(bindableView, attrName);
                            updateAttrMethod.invoke(bindableView, value);
                        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                            e.printStackTrace();
                        }

                        // スコープに登録するなら
                        if(enrollScope) {
                            // 式がメソッドや関数を含んでいたら
                            if (expression.isChangable()) {
                                // 式からビューと名前が属性名が取得できるように登録する
                                scope.enrollWithFuncExp2ViewAndAttrName(expression, bindableView, attrName);
                                // 式から評価した値と取得できるように登録する
                                scope.enrollWithFuncExp2Value(expression, value, attrName);
                            } else {
                                // メソッドや関数を含まない式を、モデル名と紐付けて登録する
                                Set<String> bindingModelNames = expression.getBindingFieldNames();
                                for (String bindingModelName : bindingModelNames) {
                                    // bindableviewがバインドしているモデルが変更したら、bindableviewのattrNameの属性を変更するように登録
                                    scope.enrollAttrAndModelAndExpression(bindingModelName, bindableView, attrName, expression);
                                }
                            }
                        }

                    }


                    // スコープに登録するなら
                    if(enrollScope) {
                        // 上の処理より後ろにすべき（初期化の値変更でScope#notifyModelChanged()が無駄に呼ばれるかもしれないから））
                        if (bindableView instanceof IEditableBindaleView) {
                            // モデル名を取得
                            String modelName = BindableViewUtil.getModelName((IEditableBindaleView) bindableView);
                            // モデル名がふられている
                            if (modelName != null && !modelName.equals("")) {
                                // 値の変更があった時の動作の定義処理を動かす
                                ((IEditableBindaleView) bindableView).defModelChanged(scope);
                            } else {
                                // モデル名がふられてないとき
                                Log.d(LOG_WARNING_TAG, String.format("Not set model(%s)", bindableView.getClass().getSimpleName()));
                            }
                        }
                    }

                    // viewにバインドしたことを知らせる
                    bindableView.onBind(scope);

                }
            }
        }
    }

    /**
     * 属性の値を更新するMethodを返す
     *
     * bindableViewのattrName属性を更新するメソッドを取得
     * @param bindableView
     * @param attrName
     * @return
     * @throws NoSuchMethodException
     */
    public static Method getUpdateAttrMethod(IBindableView bindableView, String attrName) throws NoSuchMethodException {
        // 最初を大文字にした属性名を作成
        String bigCharAttrName = Character.toTitleCase(attrName.charAt(0))+attrName.substring(1);
        return bindableView.getClass().getMethod("set" + Util.ND_METHOD_NAME + bigCharAttrName, Object.class);
    }

    /**
     * 型の比較
     * ラッパークラスでもプリミティグと比較してtrueになる
     * int.class と Integer.classもイコールになる
     * @param left
     * @param right
     * @return
     */
    public static boolean equalType(Class left, Class right){
        if(left.equals(right)){
            return true;
        } else if(left.equals(void.class) && right.equals(Void.class) || left.equals(Void.class) && right.equals(void.class)){
            return true;
        } else if(left.equals(boolean.class) && right.equals(Boolean.class) || left.equals(Boolean.class) && right.equals(boolean.class)){
            return true;
        } else if(left.equals(char.class) && right.equals(Character.class) || left.equals(Character.class) && right.equals(char.class)){
            return true;
        } else if(left.equals(byte.class) && right.equals(Byte.class) || left.equals(Byte.class) && right.equals(byte.class)){
            return true;
        } else if(left.equals(short.class) && right.equals(Short.class) || left.equals(Short.class) && right.equals(short.class)){
            return true;
        } else if(left.equals(int.class) && right.equals(Integer.class) || left.equals(Integer.class) && right.equals(int.class)){
            return true;
        } else if(left.equals(long.class) && right.equals(Long.class) || left.equals(Long.class) && right.equals(long.class)){
            return true;
        } else if(left.equals(float.class) && right.equals(Float.class) || left.equals(Float.class) && right.equals(float.class)){
            return true;
        } else if(left.equals(double.class) && right.equals(Double.class) || left.equals(Double.class) && right.equals(double.class)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * クローンを作成する
     * @return
     */
    public static<T extends Serializable> T clone(T obj){
        try {
            ByteArrayOutputStream bous = new ByteArrayOutputStream();
            ObjectOutputStream ous = new ObjectOutputStream(bous);
            ous.writeObject(obj);
            ous.close();
            ByteArrayInputStream bins = new ByteArrayInputStream(bous.toByteArray());
            T copy = (T)new ObjectInputStream(bins).readObject();
            return copy;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * viewのすべてのnd属性に値をセットする
     *
     * @param scope bindableViewが所属しているスコープ
     * @param bindableView 値をセットしたいview
     */
    public static void setValueInView(Scope scope, IBindableView bindableView) {
        // バインドする属性を取得
        BindableAttrs bindableAttrs = bindableView.getBindableAttrs();

        for (String attrName : bindableAttrs.getAttrNames()) {

            // イベント属性なら、評価しないようにする
            if (BindableViewUtil.isEventAttr(attrName)) {
                continue;
            }

            // 属性の値を文字列で取得
            String expString = bindableAttrs.getValueString(attrName);
            // 文字列の式を評価するオブジェクト作成
            EvaluatableExpression expression = new EvaluatableExpression(scope, expString);

            // 評価した値
            Object value = expression.eval();

            try {
                // 属性の値をvalueの値で初期化する
                Method updateAttrMethod = Util.getUpdateAttrMethod(bindableView, attrName);
                updateAttrMethod.invoke(bindableView, value);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * ラッパークラスをプリミティブのクラスに変換する
     * @param clazz
     * @return
     */
    public static Class wrapperClass2PrimitiveClass(Class clazz){
        if(clazz.equals(Integer.class)){
            return int.class;
        } else if(clazz.equals(Long.class)){
            return long.class;
        } else if(clazz.equals(Double.class)){
            return double.class;
        } else if(clazz.equals(Float.class)){
            return float.class;
        } else if(clazz.equals(Boolean.class)){
            return boolean.class;
        } else if(clazz.equals(Byte.class)){
            return byte.class;
        } else if(clazz.equals(Short.class)){
            return short.class;
        } else if(clazz.equals(Character.class)){
            return char.class;
        } else {
            return clazz;
        }
    }

}
