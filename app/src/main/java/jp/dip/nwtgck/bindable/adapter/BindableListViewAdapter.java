package jp.dip.nwtgck.bindable.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.dip.nwtgck.bindable.component.Arrangement;
import jp.dip.nwtgck.bindable.component.IndexedScope;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.util.Util;

/**
 * Created by Jimmy on 2015/04/03.
 */
public class BindableListViewAdapter extends BaseAdapter{
    // レイアウトをinflateするために
    private LayoutInflater mLayoutInflater;
    // listViewにつめる値
    private Arrangement mArrangement;
    // レイアウトのID
    private int mLayout;
    // 親スコープ
    private Scope mParentScope;


    private List<ViewHolder> mViewHolders = new ArrayList<>();

    // 作ったスコープ
    private Map<Integer, IndexedScope> mCreatedIndexedScopes = new HashMap<>();

    private static class ViewHolder{
        View view;
        IndexedScope indexedScope;

        public ViewHolder(View view, IndexedScope indexedScope){
            this.view = view;
            this.indexedScope = indexedScope;
        }
    }

    public BindableListViewAdapter(LayoutInflater layoutInflater, int layout, Scope parentScope, Arrangement arrangement) {
        mLayoutInflater = layoutInflater;
        mArrangement = arrangement;
        mParentScope = parentScope;
        mLayout = layout;

        // 子要素のviewとスコープを作る
        createChildViews();

    }

    /**
     * 子要素のviewとスコープを作る
     *
     * TODO データが更新されるごとに、viewとスコープの作成とバインドが行われるのを改善したい
     * TODO スコープの作成もgetView()で行っているので、もう少し最適化したい
     */
    private void createChildViews(){

//        // 親からスコープを削除
//        for(ViewHolder viewHolder: mViewHolders){
//            mParentScope.removeChild(viewHolder.indexedScope);
//        }
//
//        // viewとスコープの保存をリセット
//        mViewHolders.clear();
//
//        // スコープとviewを作成し、バインドする
//        for(int i = 0; i < mArrangement.size(); i++){
//            ViewGroup v = (ViewGroup) mLayoutInflater.inflate(mLayout, null);
//            // indexからスコープ作成
//            IndexedScope indexedScope = new IndexedScope(mParentScope, mArrangement, i);
//            // vとindexedScopeを結びつける
//            Util.bindViewsAndModels(v, indexedScope, true);
//            // 親のスコープにこのスコープを追加
//            mParentScope.addChildScope(indexedScope);
//
//            // viewとスコープを保存しておく
//            mViewHolders.add(new ViewHolder(v, indexedScope));
//        }

//        int arrangeSize = mArrangement.size();
//        int viewHolderSize = mViewHolders.size();
//
//        // データの方が保存しているviewより長い時
//        if(arrangeSize > viewHolderSize){
//            for(int i = viewHolderSize; i < arrangeSize; i++){
//                ViewGroup v = (ViewGroup) mLayoutInflater.inflate(mLayout, null);
//                // indexからスコープ作成
//                IndexedScope indexedScope = new IndexedScope(mParentScope, mArrangement, i);
//                // vとindexedScopeを結びつける
//                Util.bindViewsAndModels(v, indexedScope, true);
//                // 親のスコープにこのスコープを追加
//                mParentScope.addChildScope(indexedScope);
//
//                // viewとスコープを保存しておく
//                mViewHolders.add(new ViewHolder(v, indexedScope));
//            }
//            // データのほうが保存しているviewより短い時
//        } else if(arrangeSize < viewHolderSize){
//            for(int i = arrangeSize; i < viewHolderSize; i++){
//                // 親スコープから削除すべきに子スコープを取得
//                IndexedScope removingScope = mViewHolders.get(i).indexedScope;
//                mParentScope.removeChild(removingScope);
//            }
//        }
//
//        for(int i = 0; i < mArrangement.size(); i++){
//            // viewHolderからスコープとviewを取得
//            ViewHolder viewHolder = mViewHolders.get(i);
//            IndexedScope indexedScope = viewHolder.indexedScope;
//            View view = viewHolder.view;
//
//            // スコープのarrangementを更新する
//            indexedScope.updateArrangement(mArrangement);
////            Util.bindViewsAndModels(view, indexedScope);
//            Util.bindViewsAndModels(view, indexedScope, false);
//        }

    }

    /**
     * データの更新
     * @param arrangement
     */
    public void updateData(Arrangement arrangement){
        mArrangement = arrangement;

        // 子要素のviewとスコープを作る
        createChildViews();

        // 変更を知らせる
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mArrangement.size();
    }

    @Override
    public Object getItem(int i) {
        return mArrangement.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        // リサイクルできないなら、view作る
        if(view == null){
            view = mLayoutInflater.inflate(mLayout, null);
        }

        // 作っていたスコープなら、親スコープから削除
        if(mCreatedIndexedScopes.containsKey(position)){
            IndexedScope createdIndexedScope = mCreatedIndexedScopes.get(position);
            mParentScope.removeChild(createdIndexedScope);
        }

        // スコープを新規作成
        IndexedScope indexedScope = new IndexedScope(mParentScope, mArrangement, position);
        mCreatedIndexedScopes.put(position, indexedScope);

        // vとindexedScopeを結びつける
        Util.bindViewsAndModels(view, indexedScope, true);
        // 親のスコープに子のスコープを追加
        mParentScope.addChildScope(indexedScope);

        return view;
    }
}
