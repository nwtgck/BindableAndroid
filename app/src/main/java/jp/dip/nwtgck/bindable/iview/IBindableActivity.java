package jp.dip.nwtgck.bindable.iview;

import jp.dip.nwtgck.bindable.component.Scope;

/**
 * Created by Jimmy on 2015/03/29.
 */
public interface IBindableActivity {
    void bindViews(int layout, Scope scope);
}
