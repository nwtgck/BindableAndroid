package jp.dip.nwtgck.bindable.iview;

import jp.dip.nwtgck.bindable.component.Scope;

/**
 * Created by Jimmy on 2015/03/29.
 *
 * 編集可能なViewに必要なメソッド
 */
public interface IEditableBindaleView extends IBindableView{

    /**
     * モデルの値を返す
     * @return
     */
    public Object getNdModel();

    /**
     * モデルの変更された時を定義
     * 変更時は、scope.notifyModelChanged(modelName, model)を呼ぶ
     */
    <S extends Scope> void defModelChanged(S scope);
}
