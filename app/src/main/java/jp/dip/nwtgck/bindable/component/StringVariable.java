package jp.dip.nwtgck.bindable.component;

import java.lang.reflect.Field;

/**
 * Created by Jimmy on 2015/04/04.
 */
public class StringVariable {
    private Field mField;
    private Object mInstance;

    public StringVariable(Object instance, String name) throws NoSuchFieldException, IllegalAccessException {
        String[] splited = name.split("\\.");
        if(splited.length == 0){
            mField = instance.getClass().getDeclaredField(name);
        } else {
            Field field = instance.getClass().getDeclaredField(splited[0]);
            Object ins = field.get(instance);
            for(int i = 1; i < splited.length; i++){
                field = ins.getClass().getDeclaredField(splited[i]);
            }
            mInstance = ins;
            mField = field;
        }
    }

    public Object get() throws IllegalAccessException {
        return mField.get(mInstance);
    }

    public void set(Object value) throws IllegalAccessException {
        mField.set(mInstance, value);
    }
}
