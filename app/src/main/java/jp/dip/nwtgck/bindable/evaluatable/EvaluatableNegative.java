package jp.dip.nwtgck.bindable.evaluatable;

import jp.dip.nwtgck.bindable.component.Operator;

/**
 * Created by Jimmy on 2015/04/01.
 */
public class EvaluatableNegative extends Evaluatable{
    private EvaluatableExpression mExpression;

    public EvaluatableNegative(EvaluatableExpression expression) {
        this.mExpression = expression;
    }

    @Override
    public Object eval() {
        // 0 - evalをして負の数にする
        return Operator.minus(0, mExpression.eval());
    }

    @Override
    public boolean isChangable() {
        return mExpression.isChangable();
    }
}
