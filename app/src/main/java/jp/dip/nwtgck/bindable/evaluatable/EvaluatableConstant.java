package jp.dip.nwtgck.bindable.evaluatable;

/**
 * Created by Jimmy on 2015/04/01.
 *
 * 評価可能な定数
 */
public class EvaluatableConstant extends Evaluatable {
    private Object mValue;

    public EvaluatableConstant(Object value){
        mValue = value;
    }

    @Override
    public Object eval() {
        return mValue;
    }

    @Override
    public boolean isChangable() {
        return false;
    }
}
