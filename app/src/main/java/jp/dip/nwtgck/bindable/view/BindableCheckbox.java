package jp.dip.nwtgck.bindable.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;

import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.util.BindableViewUtil;
import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.iview.IEditableBindaleView;

/**
 * Created by Jimmy on 2015/03/29.
 */
public class BindableCheckbox extends CheckBox implements IEditableBindaleView{
    private BindableAttrs mBindableAttrs;
    private String mModelName;


    public BindableCheckbox(Context context) {
        super(context);
        init(context, null);
    }

    public BindableCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BindableCheckbox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        mBindableAttrs = new BindableAttrs(attrs);
//        mModelName = BindableViewUtil.getModelNameFromAttr(context, attrs);
    }

    @Override
    public <S extends Scope> void defModelChanged(final S scope) {
        // クリックされた時に発行
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                scope.notifyModelChanged(BindableCheckbox.this);
            }
        });
//        setOnCheckedChangeListener(new OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                scope.notifyModelChanged(BindableCheckbox.this);
//            }
//        });
    }

    @Override
    public void setNdModel(Object model) {
        // 引数が不正なとき
        if(!(model instanceof Boolean)){
            throw new IllegalArgumentException("model must be boolean");
        }
        setChecked((boolean)model);
    }

    @Override
    public Object getNdModel() {
        return isChecked();
    }

    @Override
    public BindableAttrs getBindableAttrs() {
        return mBindableAttrs;
    }

    @Override
    public void onBind(Scope scope) {

    }

    @Override
    public void onStartBinding(Scope scope) {

    }


    /**
     * 文字をセットする
     * @param text
     */
    public void setNdText(Object text){
        BindableViewUtil.setNdText(this, text);
    }

    /**
     * viewを非表示にする
     * @param isHide
     */
    public void setNdHide(Object isHide){
        BindableViewUtil.setNdHide(this, isHide);
    }
}
