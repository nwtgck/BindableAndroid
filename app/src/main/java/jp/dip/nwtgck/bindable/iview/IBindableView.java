package jp.dip.nwtgck.bindable.iview;

import jp.dip.nwtgck.bindable.component.BindableAttrs;
import jp.dip.nwtgck.bindable.component.Scope;

/**
 * バインド可能なViewが実装すべきメソッド
 * Created by Jimmy on 2015/03/28.
 */
public interface IBindableView {
    /**
     * モデルの設定
     * @param model
     */
    public void setNdModel(Object model);

    /**
     * バインドする属性を取得
     * @return
     */
    public BindableAttrs getBindableAttrs();

    /**
     * モデルとバインドした時に呼ぶ
     * @param scope
     */
    public void onBind(Scope scope);

    public void onStartBinding(Scope scope);
}
