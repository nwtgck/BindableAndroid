package jp.dip.nwtgck;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

import jp.dip.nwtgck.bindable.component.Scope;
import jp.dip.nwtgck.bindable.iview.IBindableActivity;
import jp.dip.nwtgck.bindable.util.Util;


public class MainActivity extends ActionBarActivity implements IBindableActivity{

    static class Work implements Serializable{
        public String title;
        public boolean fin = false;

        Work(String title) {
            this.title = title;
        }

        @Override
        public String toString() {
            return title +" "+fin;
        }
    }

    private Scope scope = new Scope(){
        String text = "";
        boolean checked = false;
        ArrayList<String> texts = new ArrayList<>();
        int itemId = R.layout.item;

        int length(){
            return text.length();
        }

        void addText(){
            if(text.replaceAll("\\s", "").equals("")) {
                Toast.makeText(getApplicationContext(), "Input text!", Toast.LENGTH_SHORT).show();
            } else {
                texts.add(text);
//                text = "";
            }
        }

        void removeText(int index){
            texts.remove(index);
            int mm = 0;
        }

//        String msg = "";
//
//        ArrayList<String> msgs = new ArrayList<>();
//
//        void addMsg(){
//            msgs.add(msg);
//        }
//
//        int itemId = R.layout.item;
//
//        int MAX_LEN = 20;
//        String text = "";
//        Work work = new Work("text");
//        int calledCount = 0;
//        boolean isHide = false;
//
//        ArrayList<Work> works = new ArrayList<Work>();
//
////        Arrangement arrange = new Arrangement("work", works);
//
//        // リストの項目のレイアウト
//        int listItem = R.layout.list_item;
//
////
////        String count(String ch){
////            int count = 0;
////            for(int i = 0; i < text.length(); i++){
////                if(text.charAt(i) == ch.charAt(0)) count++;
////            }
////            return ch+":    "+count;
////        }
//
////        int getCount(int n){
////            return n;
////        }
////
////        String otherCount(String ch){
////            int count = 0;
////            for(int i = 0; i < text.length(); i++){
////                if(text.charAt(i) != ch.charAt(0)) count++;
////            }
////            return "Other:  "+count;
////        }
//
////        String getText(){
////            return text;
////        }
////
////        void onClick(){
////            isHide = !isHide;
////            Toast.makeText(getApplicationContext(), "onClicked", Toast.LENGTH_SHORT).show();
////        }
//
//
////        int rest(){
////            return MAX_LEN - text.length();
////        }
////
////        ArrayList getWorks(){
////            return works;
////        }
//
//        void add(){
//            works.add(new Work(text));
//        }
//
//        void remove(int index){
//            Toast.makeText(getApplicationContext(), index+"", Toast.LENGTH_SHORT).show();
//            works.remove(index);
//        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_main);
//
//        ViewGroup parent = (ViewGroup)findViewById(R.id.parent);
//
//        for(int i = 0; i < 10; i++) {
//            View v = getLayoutInflater().inflate(R.layout.list_item, null);
//            parent.addView(v);
//        }



        bindViews(R.layout.activity_main, scope);



//        setContentView(R.layout.activity_main);
//
//        BindableListView listView = (BindableListView)findViewById(R.id.list_view);
//        String[] works = {"Report", "Recording"};
//        Arrangement arrangement = new Arrangement("work", works);
//
//        listView.setNdModel(arrangement);


//        BindableListViewAdapter listViewAdapter = new BindableListViewAdapter(this, arrangement);

//        listView.setAdapter(listViewAdapter);
    }


    /**
     * レイアウトのViewとscopeの値を結びつける
     * @param layout
     * @param scope
     */
    @Override
    public void bindViews(int layout, Scope scope) {
        Util.bindActivityModelsAndViews(this, layout, scope);
    }
}
