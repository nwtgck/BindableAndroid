package jp.dip.nwtgck.bindable.evaluatable;

import junit.framework.TestCase;

import java.io.Serializable;
import java.util.ArrayList;

import jp.dip.nwtgck.bindable.component.Scope;

public class EvaluatableExpressionTest extends TestCase {
    public void testName() throws Exception {

        class Work implements Serializable{
            int finCount = 100;

        }

        class MyScope extends Scope{
            int get(int n){
                return -n;
            }

            ArrayList<String> works = new ArrayList<>();
        };

        MyScope scope = new MyScope();
        scope.works.add("test");
        scope.works.add("report");

//        int n = --5;

        EvaluatableExpression expression = new EvaluatableExpression(scope, "work: works");
        Object eval = expression.eval();


        int mm = 0;

    }
}