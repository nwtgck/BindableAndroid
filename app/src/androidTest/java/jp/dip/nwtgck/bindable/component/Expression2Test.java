package jp.dip.nwtgck.bindable.component;

import junit.framework.TestCase;

public class Expression2Test extends TestCase {
    public void testName() throws Exception {
        Scope scope = new Scope(){
            String text = "lll";
            int MAX_LEN = 2;
        };

        Expression2 expression2 = new Expression2(scope, "MAX_LEN - text.length()");
        assertEquals(expression2.getEvalatable().eval(), -1);
    }
}